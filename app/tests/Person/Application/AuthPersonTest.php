<?php

namespace App\Tests\Person\Application;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;

/**
 * Class ShowPersonTest
 */
class AuthPersonTest extends WebTestCase
{
    public function setUp(): void
    {
        $process = new Process(['php', 'bin/console', 'd:f:l', '-n']);
        $process->run();
    }

    public function testCanAuth()
    {
        $client = self::createClient();

        $client->request('GET', '/me', [], [], [], json_encode(['username' => 'selmarinel', 'password' => '123']));

        self::assertEquals(200, $client->getResponse()->getStatusCode());

    }
}