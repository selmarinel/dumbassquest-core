<?php

namespace App\Infrastructure\Person\Model;

use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Domain\Person\Model\Profile\SetProfileInterface;
use App\Domain\Person\Model\User\SetUserInterface;
use JMS\Serializer\Annotation as Serializer;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class Profile
 *
 * @ORM\Entity(repositoryClass=ProfileRepository::class)
 */
class Profile implements GetProfileInterface, SetProfileInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     *
     * @var UuidInterface
     *
     * @Serializer\Type("uuid")
     *
     * @Serializer\Groups({"read"})
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string|null
     *
     * @Serializer\Groups({"read"})
     */
    private ?string $firstName = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string|null
     *
     * @Serializer\Groups({"read"})
     */
    private ?string $lastName = null;

    /**
     * @ORM\Column(type="string", length=10)
     *
     * @var string
     *
     * @Serializer\Groups({"read"})
     */
    private ?string $gender;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var DateTime|null
     *
     * @Serializer\Groups({"read"})
     *
     * @Serializer\Type("DateTime")
     */
    private ?DateTime $birthDate = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string|null
     *
     * @Serializer\Groups({"read"})
     */
    private ?string $country = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string|null
     *
     * @Serializer\Groups({"read"})
     */
    private ?string $city = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string|null
     *
     * @Serializer\Groups({"read"})
     */
    private ?string $address1 = null;

    /**
     * @ORM\Column(type="text")
     *
     * @var string
     *
     * @Serializer\Groups({"read"})
     */
    private string $aboutMe = '';

    /**
     * @ORM\OneToOne(targetEntity="User", fetch="EAGER", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     * @var User
     *
     * @Serializer\Groups({"read"})
     */
    private User $user;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $avatar = null;

    /**
     * @return UuidInterface
     */
    public function getId(): string
    {
        return $this->id->toString();
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): SetProfileInterface
    {
        $this->id = Uuid::getFactory()->fromString($id);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     *
     * @return self
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     *
     * @return self
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     *
     * @return self
     */
    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getBirthDate(): ?DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param DateTime|null $birthDate
     *
     * @return self
     */
    public function setBirthDate(?DateTime $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     *
     * @return self
     */
    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     *
     * @return self
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    /**
     * @param string|null $address1
     *
     * @return self
     */
    public function setAddress1(?string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * @return string
     */
    public function getAboutMe(): string
    {
        return $this->aboutMe;
    }

    /**
     * @param string $aboutMe
     *
     * @return self
     */
    public function setAboutMe(string $aboutMe): self
    {
        $this->aboutMe = $aboutMe;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param SetUserInterface $user
     *
     * @return self
     */
    public function setUser(SetUserInterface $user): SetProfileInterface
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->getUser()->getId();
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @param string|null $avatar
     *
     * @return self
     */
    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }
}
