<?php

declare(strict_types=1);

namespace App\Infrastructure\Person\Model;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 */
class UserExperience
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     *
     * @Serializer\Groups({"read"})
     *
     * @Serializer\Type("uuid")
     */
    private UuidInterface $id;

    /**
     * @ORM\OneToOne(targetEntity="User", fetch="EAGER", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     * @Serializer\Groups({"read"})
     */
    private User $user;

    /**
     * @ORM\Column(type="bigint")
     */
    private int $experience = 0;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id->toString();
    }

    /**
     * @param UuidInterface $id
     *
     * @return self
     */
    public function setId(UuidInterface $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int
     */
    public function getExperience(): int
    {
        return $this->experience;
    }

    /**
     * @param int $experience
     *
     * @return self
     */
    public function setExperience(int $experience): self
    {
        $this->experience = $experience;
        return $this;
    }
}
