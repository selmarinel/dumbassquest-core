<?php

namespace App\Infrastructure\Person\Model;

use App\Domain\Person\Model\User\GetUserInterface;
use App\Domain\Person\Model\User\SetUserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(
 *     name="users",
 *     indexes={@ORM\Index(name="index_name", columns={"username"})},
 *     uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_user", columns={"email", "phone"}),
 *      @ORM\UniqueConstraint(name="unique_name", columns={"username"})
 * })
 */
class User implements UserInterface, GetUserInterface, SetUserInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     *
     * @var UuidInterface
     *
     * @Serializer\Groups({"read"})
     *
     * @Serializer\Type("uuid")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     *
     * @Serializer\Groups({"read"})
     */
    private string $username;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private ?string $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private ?string $phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     *
     * @Serializer\Exclude()
     */
    private string $password;

    /**
     * @ORM\OneToMany(targetEntity="PremiumFeature", mappedBy="user", cascade={"persist"})
     *
     * @var PremiumFeature[]
     */
    private $features;

    /**
     * @ORM\OneToOne(targetEntity="UserExperience", mappedBy="user", cascade={"persist"})
     */
    private ?UserExperience $userExperience = null;

    public function __construct()
    {
        $this->features = new ArrayCollection();
    }

    public function getExperience(): int
    {
        if (null === $this->userExperience) {
            return 0;
        }

        return $this->userExperience->getExperience();
    }

    /**
     * @return UserExperience|null
     */
    public function getUserExperience(): ?UserExperience
    {
        return $this->userExperience;
    }

    /**
     * @param null|UserExperience $userExperience
     *
     * @return self
     */
    public function setUserExperience(?UserExperience $userExperience): self
    {
        $this->userExperience = $userExperience;
        return $this;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): string
    {
        return $this->id->toString();
    }

    /**
     * @param string $id
     *
     * @return SetUserInterface
     */
    public function setId(string $id): SetUserInterface
    {
        $this->id = Uuid::getFactory()->fromString($id);

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return SetUserInterface|self
     */
    public function setUsername(string $username): SetUserInterface
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return SetUserInterface|self
     */
    public function setEmail(?string $email): SetUserInterface
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return SetUserInterface|self
     */
    public function setPhone(?string $phone): GetUserInterface
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     *
     * @return SetUserInterface|self
     */
    public function setPassword(?string $password): SetUserInterface
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return PremiumFeature[]|PersistentCollection
     */
    public function getFeatures(): PersistentCollection
    {
        return $this->features;
    }

    /**
     * @param PremiumFeature $feature
     *
     * @return self
     */
    public function addFeature(PremiumFeature $feature): self
    {
        $this->features[] = $feature;

        return $this;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return [];
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt(): ?string
    {
        return '';
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }
}