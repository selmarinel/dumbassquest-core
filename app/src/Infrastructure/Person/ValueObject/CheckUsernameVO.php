<?php

declare(strict_types=1);

namespace App\Infrastructure\Person\ValueObject;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use App\Infrastructure\Person\Validation\Constraint as AppAssert;

class CheckUsernameVO
{
    /**
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank()
     * @AppAssert\UniqueUsernameConstraint()
     */
    private string $username;

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;


        return $this;
    }
}
