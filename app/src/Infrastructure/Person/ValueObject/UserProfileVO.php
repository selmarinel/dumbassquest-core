<?php

namespace App\Infrastructure\Person\ValueObject;

use App\Domain\Person\ValueObject\ProfileVOInterface;
use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use App\Domain\Person\Dictionary\Gender;

/**
 * Class UserProfileVO
 */
class UserProfileVO implements ProfileVOInterface
{
    /**
     * @var string|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\Type("string")
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\Type("string")
     */
    private $lastName;

    /**
     * @var string|null
     *
     * @Assert\Choice(callback={Gender::class, "getGenders"})
     *
     * @Serializer\Type("string")
     */
    private $gender;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\Type("string")
     */
    private $address1;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\Type("string")
     */
    private $country;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\Type("string")
     */
    private $city;

    /**
     * @var string|null
     *
     * @Serializer\Exclude()
     */
    private $userId;

    /**
     * @var DateTime|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\Type("DateTime<'Y-m-d'>")
     */
    private $birthDate;

    /**
     * @var string|null
     *
     * @Assert\Type("string")
     *
     * @Serializer\Type("string")
     */
    private $aboutMe;

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     *
     * @return self
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     *
     * @return self
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string|null $gender
     *
     * @return self
     */
    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    /**
     * @param string|null $address1
     *
     * @return self
     */
    public function setAddress1(?string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     *
     * @return self
     */
    public function setCountry(?string $country): self
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     *
     * @return self
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserId(): ?string
    {
        return $this->userId;
    }

    /**
     * @param string|null $userId
     *
     * @return self
     */
    public function setUserId(?string $userId): self
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getBirthDate(): ?DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param DateTime|null $birthDate
     *
     * @return self
     */
    public function setBirthDate(?DateTime $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAboutMe(): ?string
    {
        return $this->aboutMe;
    }

    /**
     * @param string|null $aboutMe
     *
     * @return self
     */
    public function setAboutMe(?string $aboutMe): self
    {
        $this->aboutMe = $aboutMe;

        return $this;
    }
}