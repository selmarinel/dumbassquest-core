<?php

declare(strict_types=1);

namespace App\Infrastructure\Person\ValueObject;

use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Domain\Person\ValueObject\PersonListVOInterface;

/**
 * Class PersonListVO.
 */
class PersonListVO implements PersonListVOInterface
{
    /**
     * @var int
     */
    private $total;

    /**
     * @var array
     */
    private $items;

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return self
     */
    public function setTotal(int $total): PersonListVOInterface
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return GetProfileInterface[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param GetProfileInterface[] $items
     *
     * @return self
     */
    public function setItems(array $items): PersonListVOInterface
    {
        $this->items = $items;

        return $this;
    }
}
