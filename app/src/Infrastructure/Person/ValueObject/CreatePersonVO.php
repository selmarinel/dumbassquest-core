<?php

namespace App\Infrastructure\Person\ValueObject;

use App\Domain\Person\ValueObject\UserVOInterface;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use App\Infrastructure\Person\Validation\Constraint as AppAssert;

/**
 * Class CreatePersonDTO
 */
class CreatePersonVO implements UserVOInterface
{
    /**
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank()
     * @AppAssert\UniqueUsernameConstraint()
     */
    private string $username;

    /**
     * @Serializer\Type("string")
     */
    private ?string $email = null;

    /**
     * @Serializer\Type("string")
     */
    private ?string $phone = null;

    /**
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    private string $password;

    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return self
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return self
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return self
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return self
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
