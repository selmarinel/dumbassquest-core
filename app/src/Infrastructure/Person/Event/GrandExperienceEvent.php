<?php

declare(strict_types=1);

namespace App\Infrastructure\Person\Event;

use App\Domain\Person\Model\User\GetUserInterface;

class GrandExperienceEvent
{
    private GetUserInterface $user;
    private int $exp;

    public function __construct(GetUserInterface $user, int $exp)
    {
        $this->user = $user;
        $this->exp = $exp;
    }

    public function getUser(): GetUserInterface
    {
        return $this->user;
    }

    public function getExp(): int
    {
        return $this->exp;
    }
}
