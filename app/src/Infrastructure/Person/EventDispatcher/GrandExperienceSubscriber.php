<?php

declare(strict_types=1);

namespace App\Infrastructure\Person\EventDispatcher;

use App\Infrastructure\Person\Event\GrandExperienceEvent;
use App\Infrastructure\Person\Model\UserExperience;
use App\Infrastructure\Person\Repository\UserExperienceRepository;
use Predis\Client;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Lock\Store\RedisStore;
use Symfony\Component\Lock\Store\RetryTillSaveStore;
use Symfony\Component\Lock\Factory;

class GrandExperienceSubscriber implements EventSubscriberInterface
{
    private UserExperienceRepository $repository;

    public function __construct(UserExperienceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            GrandExperienceEvent::class => 'onGrandExperience'
        ];
    }

    public function onGrandExperience(GrandExperienceEvent $event): void
    {
        $user = $event->getUser();

        $store = new RedisStore(new Client(getenv('REDIS_URL')));
        $store = new RetryTillSaveStore($store);
        $factory = new Factory($store);
        $lock = $factory->createLock('grand_experience', 30);
        $lock->acquire();

        try {
            /** @var UserExperience $userExperience */
            $userExperience = $this->repository->findOneBy(['user' => $user]);
            $userExperience->setExperience($user->getExperience() + $event->getExp());

            $this->repository->save($userExperience);

        } finally {
            $lock->release();
        }

    }
}
