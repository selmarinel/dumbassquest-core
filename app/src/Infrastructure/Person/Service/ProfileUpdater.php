<?php

namespace App\Infrastructure\Person\Service;

use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Domain\Person\Model\User\SetUserInterface;
use App\Domain\Person\Repository\ProfileRepositoryInterface;
use App\Domain\Person\Service\PersonFetcherInterface;
use App\Domain\Person\Service\ProfileUpdaterInterface;
use App\Domain\Person\ValueObject\ProfileVOInterface;
use App\Infrastructure\Person\Model\Profile;

/**
 * Class ProfileUpdater
 */
class ProfileUpdater implements ProfileUpdaterInterface
{
    /**
     * @var PersonFetcher
     */
    private $personFetcher;

    /**
     * @var ProfileRepositoryInterface
     */
    private $profileRepository;

    /**
     * @param PersonFetcherInterface $personFetcher
     * @param ProfileRepositoryInterface $profileRepository
     */
    public function __construct(PersonFetcherInterface $personFetcher, ProfileRepositoryInterface $profileRepository)
    {
        $this->personFetcher = $personFetcher;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param ProfileVOInterface $profileVO
     *
     * @return GetProfileInterface
     */
    public function updateProfile(ProfileVOInterface $profileVO): GetProfileInterface
    {
        if (!$profile = $this->personFetcher->fetchProfile()) {
            $profile = new Profile();
            /** @var SetUserInterface $user */
            $user = $this->personFetcher->fetchUser();
            $profile->setUser($user);
        }

        $profile
            ->setFirstName($profileVO->getFirstName() ?: $profile->getFirstName())
            ->setLastName($profileVO->getLastName() ?: $profile->getLastName())
            ->setBirthDate($profileVO->getBirthDate() ?: $profile->getBirthDate())
            ->setGender($profileVO->getGender() ?: $profile->getGender())
            ->setAddress1($profileVO->getAddress1() ?: $profile->getAddress1())
            ->setCountry($profileVO->getCountry() ?: $profile->getCountry())
            ->setCity($profileVO->getCity() ?: $profile->getCity())
            ->setAboutMe($profileVO->getAboutMe() ?: $profile->getAboutMe());

        $this->profileRepository->save($profile);

        //todo add event dispatch

        return $profile;
    }
}