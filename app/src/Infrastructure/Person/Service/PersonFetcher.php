<?php

namespace App\Infrastructure\Person\Service;

use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Domain\Person\Model\User\GetUserInterface;
use App\Domain\Person\Repository\ProfileRepositoryInterface;
use App\Domain\Person\Service\PersonFetcherInterface;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class PersonFetcher
 */
class PersonFetcher implements PersonFetcherInterface
{
    private TokenStorageInterface $tokenStorage;

    private ProfileRepositoryInterface $profileRepository;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        ProfileRepositoryInterface $profileRepository
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @return GetUserInterface
     */
    public function fetchUser(): GetUserInterface
    {
        $token = $this->tokenStorage->getToken();

        if (null === $token) {
            throw new LogicException('Token not provided');
        }

        /** @var GetUserInterface $user */
        $user = $token->getUser();

        return $user;
    }

    /**
     * @return GetProfileInterface
     */
    public function fetchProfile(): GetProfileInterface
    {
        return $this->fetchProfileByUserId($this->fetchUser()->getId());
    }

    /**
     * @param string $userId
     *
     * @return GetProfileInterface
     */
    public function fetchProfileByUserId(string $userId): GetProfileInterface
    {
        return $this->profileRepository->findByUserId($userId);
    }
}