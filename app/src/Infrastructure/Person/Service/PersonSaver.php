<?php

namespace App\Infrastructure\Person\Service;

use App\Domain\Person\Dictionary\Gender;
use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Domain\Person\Repository\ProfileRepositoryInterface;
use App\Domain\Person\Service\PersonSaverInterface;
use App\Domain\Person\ValueObject\UserVOInterface;
use App\Infrastructure\Person\Model\Profile;
use App\Infrastructure\Person\Model\User;
use App\Infrastructure\Person\Model\UserExperience;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class PersonSaver
 */
class PersonSaver implements PersonSaverInterface
{
    private UserPasswordEncoderInterface $passwordEncoder;

    private ProfileRepositoryInterface $profileRepository;

    /**
     * PersonSaver constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param ProfileRepositoryInterface $profileRepository
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, ProfileRepositoryInterface $profileRepository)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param UserVOInterface $userVO
     * @return GetProfileInterface
     */
    public function savePerson(UserVOInterface $userVO): GetProfileInterface
    {
        $user = new User();
        $user->setUsername($userVO->getUsername())
            ->setEmail($userVO->getEmail())
            ->setPhone($userVO->getPhone())
            ->setPassword($this->passwordEncoder->encodePassword($user, $userVO->getPassword()));

        $userExperience = new UserExperience();
        $userExperience
            ->setExperience(0)
            ->setUser($user);

        $profile = new Profile();
        $profile
            ->setUser($user)
            ->setGender(Gender::GENDER_UNDEFINED);

        return $this->profileRepository->save($profile);
    }
}