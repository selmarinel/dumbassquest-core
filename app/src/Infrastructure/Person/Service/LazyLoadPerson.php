<?php

declare(strict_types=1);

namespace App\Infrastructure\Person\Service;

use App\Domain\Person\ValueObject\PersonListVOInterface;
use App\Infrastructure\Person\DataTransferObject\PersonFilterRequestDTO;
use App\Infrastructure\Person\Model\Profile;
use App\Infrastructure\Person\ValueObject\PersonListVO;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class LazyLoadPerson.
 */
class LazyLoadPerson
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * LazyLoadPerson constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param PersonFilterRequestDTO $personFilterRequestDTO
     *
     * @return PersonListVOInterface
     */
    public function search(PersonFilterRequestDTO $personFilterRequestDTO): PersonListVOInterface
    {
        $items = $this->entityManager
            ->createQueryBuilder()
            ->select('p')
            ->from(Profile::class, 'p')
            ->setMaxResults($personFilterRequestDTO->getTake())
            ->setFirstResult($personFilterRequestDTO->getOffset())
            ->getQuery()
            ->getResult();

        $personListVO = new PersonListVO();

        return $personListVO
            ->setItems($items)
            ->setTotal(0);
    }
}
