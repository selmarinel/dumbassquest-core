<?php

declare(strict_types=1);

namespace App\Infrastructure\Person\DataTransferObject;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class PersonFilterRequestDTO.
 */
class PersonFilterRequestDTO
{
    /**
     * @Serializer\Type("integer")
     * @Serializer\Groups("read")
     *
     * @var int
     */
    private int $take = 20;

    /**
     * @Serializer\Type("integer")
     * @Serializer\Groups("read")
     *
     * @var int
     */
    private int $page = 1;

    /**
     * @return int
     */
    public function getTake(): int
    {
        return $this->take;
    }

    /**
     * @param int $take
     *
     * @return self
     */
    public function setTake(int $take): self
    {
        $this->take = $take;

        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     *
     * @return self
     */
    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        $offset = ($this->getPage() - 1) * $this->getTake();

        return ($offset < 0) ? $offset : 0;
    }
}
