<?php

namespace App\Infrastructure\Person\Fixtures;

use App\Domain\Person\Dictionary\Feature;
use App\Domain\Person\Dictionary\Gender;
use App\Infrastructure\Person\Model\PremiumFeature;
use App\Infrastructure\Person\Model\Profile;
use App\Infrastructure\Person\Model\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserFixture
 */
class UserFixture extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i <= 10; $i++) {
            $this->create($manager);
        }

        $manager->flush();
    }

    private function create(ObjectManager $manager)
    {
        $faker = Factory::create();
        $feature = new PremiumFeature();
        $feature->setAvailable(true);
        $feature->setName(Feature::HIGHLIGHT_AVATAR);

        $user = $manager->getRepository(User::class)->findOneBy(['username' => $faker->userName]);

        if ($user) {
            return;
        }

        $user = new User();
        $user->setUsername($faker->userName);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            '123'
        ));

        $feature->setUser($user);
        if (random_int(0,1) === 1){
            $user->addFeature($feature);
        }

        $faker = Factory::create();

        $profile = new Profile();
        $profile->setUser($user)
            ->setGender(Gender::GENDER_UNDEFINED)
            ->setFirstName($faker->firstName)
            ->setLastName($faker->lastName)
            ->setCountry($faker->country)
            ->setAddress1($faker->address)
            ->setAboutMe($faker->text)
            ->setAvatar($faker->imageUrl(150, 150));


        $manager->persist($profile);
    }
}
