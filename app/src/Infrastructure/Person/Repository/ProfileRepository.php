<?php

namespace App\Infrastructure\Person\Repository;

use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Domain\Person\Model\Profile\SetProfileInterface;
use App\Domain\Person\Model\User\SetUserInterface;
use App\Domain\Person\Repository\ProfileRepositoryInterface;
use App\Infrastructure\Person\Model\Profile;
use App\Infrastructure\Person\Model\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class ProfileRepository extends ServiceEntityRepository implements ProfileRepositoryInterface
{
    /**
     * UserRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Profile::class);
    }

    /**
     * @param string $userId
     * @return GetProfileInterface
     *
     * @throws ORMException
     */
    public function prepareProfile(string $userId): GetProfileInterface
    {
        $profile = new Profile();

        /** @var SetUserInterface $referenceUser */
        $referenceUser = $this->_em->getReference(User::class, $userId);
        $profile->setUser($referenceUser);

        return $profile;
    }

    /**
     * @param string $userId
     *
     * @return GetProfileInterface
     */
    public function findByUserId(string $userId): GetProfileInterface
    {
        try {
            //todo add eager ?
            //todo add simplify search
            $profile = $this->createQueryBuilder('p')
                ->join('p.user', 'u')
                ->where('u.id = :userId')
                ->setParameter('userId', $userId)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

            return $profile;
        } catch (NonUniqueResultException $exception) {
            return null;
        }
    }

    /**
     * @param SetProfileInterface|GetProfileInterface $profile
     *
     * @return GetProfileInterface
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(SetProfileInterface $profile): GetProfileInterface
    {
        $this->_em->persist($profile);
        $this->_em->flush($profile);

        return $profile;
    }
}