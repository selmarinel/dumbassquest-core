<?php

namespace App\Infrastructure\Person\Repository;

use App\Domain\Person\Model\User\GetUserInterface;
use App\Domain\Person\Model\User\SetUserInterface;
use App\Domain\Person\Repository\UserRepositoryInterface;
use App\Infrastructure\Person\Model\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Ramsey\Uuid\Converter\NumberConverterInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class UserRepository
 */
class UserRepository extends ServiceEntityRepository implements UserRepositoryInterface
{
    /**
     * @var NumberConverterInterface
     */
    private $converter;

    /**
     * UserRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        //todo make factory
        parent::__construct($registry, User::class);
    }

    /**
     * @param SetUserInterface|GetUserInterface $user
     *
     * @return GetUserInterface
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(SetUserInterface $user): GetUserInterface
    {
        $this->_em->persist($user);
        $this->_em->flush($user);

        return $user;
    }

    /**
     * @param string $id
     *
     * @return GetUserInterface|null
     */
    public function findById(string $id): ?GetUserInterface
    {
        /** @var GetUserInterface $user */
        $user = $this->find($id);

        return $user;
    }

    /**
     * @param string $username
     *
     * @return GetUserInterface|null
     */
    public function findByUsername(string $username): ?GetUserInterface
    {
        /** @var GetUserInterface $user */
        $user = $this->findOneBy(['username' => $username]);

        return $user;
    }
}