<?php

declare(strict_types=1);

namespace App\Infrastructure\Person\Repository;

use App\Infrastructure\Person\Model\UserExperience;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;

class UserExperienceRepository extends EntityRepository
{
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, new Mapping\ClassMetadata(UserExperience::class));
    }

    public function save(UserExperience $experience):UserExperience
    {
        $this->_em->persist($experience);
        $this->_em->flush($experience);

        return $experience;
    }
}
