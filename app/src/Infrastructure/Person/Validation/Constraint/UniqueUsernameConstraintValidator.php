<?php

namespace App\Infrastructure\Person\Validation\Constraint;

use App\Domain\Person\Repository\UserRepositoryInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class UniqueUsernameConstraintValidator
 */
class UniqueUsernameConstraintValidator extends ConstraintValidator
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UniqueUsernameConstraintValidator constructor.
     * @param UserRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->userRepository = $repository;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UniqueUsernameConstraint) {
            return;
        }

        if ($value === null) {
            return;
        }

        if ($this->userRepository->findByUsername($value)) {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}