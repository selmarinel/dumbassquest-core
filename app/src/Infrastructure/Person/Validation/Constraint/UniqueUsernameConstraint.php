<?php

namespace App\Infrastructure\Person\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use function get_class;

/**
 * @Annotation
 */
class UniqueUsernameConstraint extends Constraint
{
    public $message = 'Person with this username already exists';

    /**
     * @return string
     */
    public function validatedBy(): string
    {
        return get_class($this) . 'Validator';
    }
}