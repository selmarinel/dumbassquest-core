<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Repository;

use App\Domain\Achievement\Model\AchievementInterface;
use App\Domain\Achievement\Model\AchievementReceivedInterface;
use App\Domain\Achievement\Repository\AchievementReceivedRepositoryInterface;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;
use MongoException;
use MongoId;

/**
 * Class AchievementUserGrandRepository.
 */
class AchievementReceivedRepository extends DocumentRepository implements AchievementReceivedRepositoryInterface
{
    /**
     * @param AchievementReceivedInterface $achievementGrand
     *
     * @return AchievementReceivedInterface
     */
    public function save(AchievementReceivedInterface $achievementGrand): AchievementReceivedInterface
    {
        $this->dm->persist($achievementGrand);
        $this->dm->flush($achievementGrand);
        $this->dm->clear($achievementGrand);

        return $achievementGrand;
    }

    /**
     * @param AchievementInterface $achievement
     * @param string $userId
     *
     * @return AchievementReceivedInterface|null
     *
     * @throws MongoException
     */
    public function findByAchievementAndUserId(AchievementInterface $achievement, string $userId): ?AchievementReceivedInterface
    {
        /** @var AchievementReceivedInterface $achievementReceived */
        $achievementReceived = $this->createQueryBuilder()
            ->field('userId')->equals($userId)
            ->field('type')->equals($achievement->getType())
            ->field('achievement.$id')->equals(new MongoId($achievement->getId()))
            ->getQuery()
            ->getSingleResult();

        return $achievementReceived;
    }

    /**
     * @param string $userId
     *
     * @return AchievementReceivedInterface[]
     */
    public function findByUserId(string $userId): array
    {
        return $this->createQueryBuilder()
            ->field('userId')->equals($userId)
            ->getQuery()
            ->toArray();
    }
}
