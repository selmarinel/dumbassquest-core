<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Repository;

use App\Domain\Achievement\Model\AchievementInterface;
use App\Domain\Achievement\Repository\AchievementRepositoryInterface;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

/**
 * Class AchievementRepository.
 */
class AchievementRepository extends DocumentRepository implements AchievementRepositoryInterface
{
    public function save(AchievementInterface $achievement): AchievementInterface
    {
        $this->dm->persist($achievement);
        $this->dm->flush($achievement);
        $this->dm->clear($achievement);

        return $achievement;
    }


    /**
     * @param string $name
     *
     * @return AchievementInterface|null
     */
    public function findByName(string $name): ?AchievementInterface
    {
        /** @var AchievementInterface $achievement */
        $achievement = $this->findOneBy(['name' => $name]);

        return $achievement;
    }

    /**
     * @return AchievementInterface[]
     */
    public function all(): array
    {
        return $this->findAll();
    }
}
