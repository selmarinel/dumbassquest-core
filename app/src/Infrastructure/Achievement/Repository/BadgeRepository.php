<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Repository;

use App\Domain\Achievement\Model\BadgeInterface;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

/**
 * Class BadgeRepository.
 */
class BadgeRepository extends DocumentRepository
{
    /**
     * @param BadgeInterface $badge
     *
     * @return BadgeInterface
     */
    public function save(BadgeInterface $badge): BadgeInterface
    {
        $this->dm->persist($badge);
        $this->dm->flush($badge);
        $this->dm->clear($badge);

        return $badge;
    }

    /**
     * @param string $name
     *
     * @return BadgeInterface|null
     */
    public function findByName(string $name): ?BadgeInterface
    {
        /** @var BadgeInterface $badge */
        $badge = $this->findOneBy(['name' => $name]);

        return $badge;
    }
}
