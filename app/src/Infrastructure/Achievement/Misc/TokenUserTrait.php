<?php

namespace App\Infrastructure\Achievement\Misc;

use RuntimeException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Trait TokenUserTrait.
 *
 * @property TokenStorageInterface $token
 */
trait TokenUserTrait
{
    /**
     * @return UserInterface
     */
    private function getUser(): UserInterface
    {
        $token = $this->token->getToken();
        if (!$token) {
            throw new RuntimeException('user.not_auth');
        }
        /** @var UserInterface $user */
        $user = $token->getUser();

        return $user;
    }
}