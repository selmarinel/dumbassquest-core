<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\DataTransferObject;

use App\Domain\Achievement\DataTransferObject\AchievementDTOInterface;
use App\Domain\Achievement\DataTransferObject\BadgeDTOInterface;

/**
 * Class BadgeDTO.
 */
class BadgeDTO implements BadgeDTOInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var AchievementDTOInterface[]
     */
    private $achievements;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $description
     *
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return AchievementDTOInterface[]
     */
    public function getAchievements(): array
    {
        return $this->achievements;
    }

    /**
     * @param AchievementDTOInterface $achievementDTO
     *
     * @return $this
     */
    public function addAchievement(AchievementDTOInterface $achievementDTO): self
    {
        $this->achievements[] = $achievementDTO;

        return $this;
    }
}
