<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\DataTransferObject;

use App\Domain\Achievement\DataTransferObject\ReceivedAchievementDTOInterface;
use DateTimeInterface;

/**
 * Class ReceivedAchievementDTO.
 */
class ReceivedAchievementDTO extends AchievementDTO implements ReceivedAchievementDTOInterface
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var float
     */
    private $progress;

    /**
     * @var DateTimeInterface|null
     */
    private $receivedAt;

    /**
     * @var DateTimeInterface
     */
    private $createdAt;

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     *
     * @return self
     */
    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return float
     */
    public function getProgress(): float
    {
        return $this->progress;
    }

    /**
     * @param float $progress
     *
     * @return self
     */
    public function setProgress(float $progress): self
    {
        $this->progress = $progress;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getReceivedAt(): ?DateTimeInterface
    {
        return $this->receivedAt;
    }

    /**
     * @param DateTimeInterface|null $receivedAt
     *
     * @return self
     */
    public function setReceivedAt(?DateTimeInterface $receivedAt): self
    {
        $this->receivedAt = $receivedAt;

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeInterface $createdAt
     *
     * @return self
     */
    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
