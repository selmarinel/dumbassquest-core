<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\DataTransferObject;

use App\Domain\Achievement\DataTransferObject\AchievementDTOInterface;
use App\Infrastructure\Achievement\Constraint\ConstrainsProgressStep;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AchievementDTO.
 */
class AchievementDTO implements AchievementDTOInterface
{
    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"read"})
     *
     * @var string
     */
    private $id;

    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"create", "read"})
     *
     * @Assert\NotBlank(groups={"create"});
     *
     * @var string
     */
    private $name;

    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"create", "read"})
     *
     * @Assert\NotBlank(groups={"create"})
     * @Assert\Type("string", groups={"create"})
     *
     * @var string
     */
    private $description;

    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"create"})
     *
     * @Assert\NotBlank(groups={"create"})
     *
     * @var string
     */
    private $type;

    /**
     * @Serializer\Type("integer")
     * @Serializer\Groups({"create"})
     *
     * @ConstrainsProgressStep(groups={"create"})
     *
     * @var int
     */
    private $stepsToAchieve;

    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"create"})
     *
     * @var string|null
     */
    private $icon;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $description
     *
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string|null $icon
     *
     * @return self
     */
    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStepsToAchieve(): ?int
    {
        return $this->stepsToAchieve;
    }

    /**
     * @param int $stepsToAchieve
     *
     * @return self
     */
    public function setStepsToAchieve(int $stepsToAchieve): self
    {
        $this->stepsToAchieve = $stepsToAchieve;

        return $this;
    }
}
