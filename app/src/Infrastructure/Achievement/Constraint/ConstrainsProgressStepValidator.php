<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Constraint;

use App\Domain\Achievement\Dictionary\AchievementStrategyType;
use App\Infrastructure\Achievement\DataTransferObject\AchievementDTO;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class ConstrainsProgressStepValidator.
 */
class ConstrainsProgressStepValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ConstrainsProgressStep) {
            throw new UnexpectedTypeException($constraint, ConstrainsProgressStep::class);
        }

        /** @var AchievementDTO $achievementDTO */
        $achievementDTO = $this->context->getObject();

        if (!$achievementDTO instanceof AchievementDTO) {
            throw new UnexpectedTypeException($constraint, AchievementDTO::class);
        }

        if (!$achievementDTO->getType() === AchievementStrategyType::PROGRESS) {
            return;
        }

        if (!$value) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}
