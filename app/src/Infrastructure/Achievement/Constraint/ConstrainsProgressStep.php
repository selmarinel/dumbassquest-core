<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Class ConstrainsProgressStep.
 * @Annotation
 */
class ConstrainsProgressStep extends Constraint
{
    public $message = 'achievement.progress.steps.max.need';
}
