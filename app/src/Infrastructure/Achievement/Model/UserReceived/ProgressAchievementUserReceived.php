<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Model\UserReceived;

use App\Domain\Achievement\Model\AchievementInterface;
use App\Infrastructure\Achievement\Model\Achievement\ProgressAchievement;
use App\Infrastructure\Achievement\Model\AchievementUserReceived;
use App\Infrastructure\Achievement\Repository\AchievementReceivedRepository;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class ProgressAchievementUserReceived.
 *
 * @ODM\Document(repositoryClass=AchievementReceivedRepository::class)
 */
class ProgressAchievementUserReceived extends AchievementUserReceived
{
    /**
     * @ODM\ReferenceOne(targetDocument=ProgressAchievement::class)
     *
     * @var ProgressAchievement
     */
    protected $achievement;

    /**
     * @ODM\Field(type="integer")
     *
     * @var int
     */
    private $stepsAchieved;

    /**
     * @return int
     */
    public function getStepsAchieved(): int
    {
        return $this->stepsAchieved;
    }

    /**
     * @param int $stepsAchieved
     *
     * @return self
     */
    public function setStepsAchieved(int $stepsAchieved): self
    {
        $this->stepsAchieved = $stepsAchieved;

        return $this;
    }

    /**
     * @return AchievementInterface|ProgressAchievement
     */
    public function getAchievement(): AchievementInterface
    {
        return $this->achievement;
    }
}
