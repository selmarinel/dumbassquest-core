<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Model\UserReceived;

use App\Domain\Achievement\Model\AchievementInterface;
use App\Infrastructure\Achievement\Model\Achievement\ActionAchievement;
use App\Infrastructure\Achievement\Model\AchievementUserReceived;
use App\Infrastructure\Achievement\Repository\AchievementReceivedRepository;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class ActionAchievementUserReceived.
 *
 * @ODM\Document(repositoryClass=AchievementReceivedRepository::class)
 */
class ActionAchievementUserReceived extends AchievementUserReceived
{
    /**
     * @ODM\ReferenceOne(targetDocument=ActionAchievement::class)
     *
     * @var ActionAchievement
     */
    protected $achievement;

    /**
     * @return AchievementInterface|ActionAchievement
     */
    public function getAchievement(): AchievementInterface
    {
        return $this->achievement;
    }
}
