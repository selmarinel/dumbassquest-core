<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Model;

use App\Domain\Achievement\Model\AchievementInterface;
use App\Domain\Achievement\Model\AchievementReceivedInterface;
use App\Infrastructure\Achievement\Model\UserReceived\ActionAchievementUserReceived;
use App\Infrastructure\Achievement\Model\UserReceived\ProgressAchievementUserReceived;
use App\Infrastructure\Achievement\Repository\AchievementReceivedRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Exception;

/**
 * Class AchievementUserGrand.
 * @ODM\Document(repositoryClass=AchievementReceivedRepository::class)
 * @ODM\InheritanceType("SINGLE_COLLECTION")
 * @ODM\DiscriminatorField("type")
 * @ODM\DiscriminatorMap({"action"=ActionAchievementUserReceived::class, "progress"=ProgressAchievementUserReceived::class})
 */
abstract class AchievementUserReceived implements AchievementReceivedInterface
{
    /**
     * @var string
     *
     * @ODM\Id()
     */
    private $id;

    /**
     * @var string
     *
     * @ODM\Field(type="string")
     */
    private $userId;

    /**
     * @var float
     *
     * @ODM\Field(type="float")
     */
    private $progress;

    /**
     * @var DateTimeInterface
     *
     * @ODM\Field(type="date")
     */
    private $createdAt;

    /**
     * @var DateTimeInterface
     *
     * @ODM\Field(type="date")
     */
    private $receivedAt;

    /**
     * @var AchievementInterface
     */
    protected $achievement;

    /**
     * AchievementUserReceived constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     *
     * @return AchievementReceivedInterface
     */
    public function setUserId(string $userId): AchievementReceivedInterface
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return float
     */
    public function getProgress(): float
    {
        return $this->progress;
    }

    /**
     * @param float $progress
     *
     * @return AchievementReceivedInterface
     */
    public function setProgress(float $progress): AchievementReceivedInterface
    {
        $this->progress = $progress;

        return $this;
    }

    /**
     * @param DateTimeInterface $receivedAt
     *
     * @return self
     */
    public function setReceivedAt(DateTimeInterface $receivedAt): AchievementReceivedInterface
    {
        $this->receivedAt = $receivedAt;

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getReceivedAt(): DateTimeInterface
    {
        return $this->receivedAt;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param AchievementInterface $achievement
     *
     * @return self
     */
    public function setAchievement(AchievementInterface $achievement): AchievementReceivedInterface
    {
        $this->achievement = $achievement;

        return $this;
    }
}
