<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Model;

use App\Domain\Achievement\Model\AchievementInterface;
use App\Domain\Achievement\Model\BadgeInterface;
use App\Infrastructure\Achievement\Repository\BadgeRepository;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class Badge.
 *
 * @ODM\Document(repositoryClass=BadgeRepository::class)
 */
class Badge implements BadgeInterface
{
    /**
     * @var string
     *
     * @ODM\Id()
     */
    private $id;

    /**
     * @var string
     *
     * @ODM\Field(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ODM\Field(type="string")
     */
    private $description;

    /**
     * @var AchievementInterface[]
     *
     * @ODM\ReferenceMany(targetDocument=Achievement::class)
     */
    private $achievements;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return AchievementInterface[]
     */
    public function getAchievements(): array
    {
        return $this->achievements;
    }

    /**
     * @param string $name
     *
     * @return BadgeInterface
     */
    public function setName(string $name): BadgeInterface
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $description
     *
     * @return BadgeInterface
     */
    public function setDescription(string $description): BadgeInterface
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param AchievementInterface $achievement
     *
     * @return BadgeInterface
     */
    public function addAchievement(AchievementInterface $achievement): BadgeInterface
    {
        $this->achievements[] = $achievement;

        return $this;
    }
}
