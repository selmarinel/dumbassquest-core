<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Model\Achievement;

use App\Domain\Achievement\Dictionary\AchievementStrategyType;
use App\Infrastructure\Achievement\Model\Achievement;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class ActionAchievement.
 *
 * @ODM\Document()
 */
class ActionAchievement extends Achievement
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return AchievementStrategyType::ACTION;
    }
}
