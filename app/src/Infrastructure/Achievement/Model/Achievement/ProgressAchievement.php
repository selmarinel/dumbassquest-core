<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Model\Achievement;

use App\Domain\Achievement\Dictionary\AchievementStrategyType;
use App\Infrastructure\Achievement\Model\Achievement;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class ProgressAchievement.
 *
 * @ODM\Document()
 */
class ProgressAchievement extends Achievement
{
    /**
     * @ODM\Field(type="integer")
     *
     * @var int
     */
    private $steps;

    /**
     * @return string
     */
    public function getType(): string
    {
        return AchievementStrategyType::PROGRESS;
    }

    /**
     * @return int
     */
    public function getSteps(): int
    {
        return $this->steps;
    }

    /**
     * @param int $steps
     *
     * @return self
     */
    public function setSteps(int $steps): self
    {
        $this->steps = $steps;

        return $this;
    }
}
