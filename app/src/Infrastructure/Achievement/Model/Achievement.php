<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Model;

use App\Domain\Achievement\Model\AchievementInterface;
use App\Infrastructure\Achievement\Model\Achievement\ActionAchievement;
use App\Infrastructure\Achievement\Model\Achievement\ProgressAchievement;
use App\Infrastructure\Achievement\Repository\AchievementRepository;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class Achievement.
 *
 * @ODM\Document(repositoryClass=AchievementRepository::class, collection="achievements")
 * @ODM\InheritanceType("SINGLE_COLLECTION")
 * @ODM\DiscriminatorField("type")
 * @ODM\DiscriminatorMap({"action"=ActionAchievement::class, "progress"=ProgressAchievement::class})
 */
abstract class Achievement implements AchievementInterface
{
    /**
     * @var string
     *
     * @ODM\Id()
     */
    private $id;

    /**
     * @ODM\Field(type="string")
     *
     * @var string
     */
    private $name;

    /**
     * @ODM\Field(type="string")
     *
     * @var string
     */
    private $description;

    /**
     * @ODM\Field(type="string")
     *
     * @var string
     */
    private $grandType;

    /**
     * @ODM\Field(type="string")
     *
     * @var string
     */
    private $icon;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getGrandType(): ?string
    {
        return $this->grandType;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string $name
     *
     * @return AchievementInterface
     */
    public function setName(string $name): AchievementInterface
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $description
     *
     * @return AchievementInterface
     */
    public function setDescription(string $description): AchievementInterface
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): AchievementInterface
    {
        $this->id = $id;

        return $this;
    }
}

