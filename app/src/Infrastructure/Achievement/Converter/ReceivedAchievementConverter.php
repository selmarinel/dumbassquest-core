<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Converter;

use App\Domain\Achievement\Model\AchievementReceivedInterface;
use App\Infrastructure\Achievement\DataTransferObject\ReceivedAchievementDTO;

/**
 * Class ReceivedAchievementConverter.
 */
class ReceivedAchievementConverter
{
    /**
     * @param AchievementReceivedInterface $achievementReceived
     *
     * @return ReceivedAchievementDTO
     */
    public static function convertToDTO(AchievementReceivedInterface $achievementReceived): ReceivedAchievementDTO
    {
        $achievement = $achievementReceived->getAchievement();
        $dto = new ReceivedAchievementDTO();

        return $dto
            ->setId($achievement->getId())
            ->setName($achievement->getName())
            ->setDescription($achievement->getDescription())
            ->setType($achievement->getType())
            ->setIcon($achievement->getIcon())
            ->setProgress($achievementReceived->getProgress())
            ->setUserId($achievementReceived->getUserId())
            ->setReceivedAt($achievementReceived->getReceivedAt())
            ->setCreatedAt($achievementReceived->getCreatedAt());

    }
}
