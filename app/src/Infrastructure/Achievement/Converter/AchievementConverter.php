<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Converter;

use App\Domain\Achievement\DataTransferObject\AchievementDTOInterface;
use App\Domain\Achievement\Model\AchievementInterface;
use App\Infrastructure\Achievement\DataTransferObject\AchievementDTO;

/**
 * Class AchievementConverter.
 */
final class AchievementConverter
{
    /**
     * @param AchievementInterface $achievement
     *
     * @return AchievementDTOInterface
     */
    public static function convertToDTO(AchievementInterface $achievement): AchievementDTOInterface
    {
        $achievementDTO = new AchievementDTO();
        $achievementDTO
            ->setId($achievement->getId())
            ->setName($achievement->getName())
            ->setDescription($achievement->getDescription())
            ->setType($achievement->getType())
            ->setIcon($achievement->getIcon());

        return $achievementDTO;
    }
}
