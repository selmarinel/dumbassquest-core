<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Service\ReceivedFetcher;

use App\Domain\Achievement\Repository\AchievementReceivedRepositoryInterface;
use App\Infrastructure\Achievement\Converter\ReceivedAchievementConverter;
use App\Infrastructure\Achievement\DataTransferObject\ReceivedAchievementDTO;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserReceivedAchievement.
 */
class UserReceivedAchievementFetcher
{
    /**
     * @var AchievementReceivedRepositoryInterface
     */
    private $achievementReceivedRepository;

    /**
     * UserReceivedAchievementFetcher constructor.
     *
     * @param AchievementReceivedRepositoryInterface $achievementReceivedRepository
     */
    public function __construct(AchievementReceivedRepositoryInterface $achievementReceivedRepository)
    {
        $this->achievementReceivedRepository = $achievementReceivedRepository;
    }

    /**
     * @param UserInterface $user
     *
     * @return ReceivedAchievementDTO[]
     */
    public function fetchReceivedAchievementsForUser(UserInterface $user): array
    {
        $achievements = [];
        foreach ($this->achievementReceivedRepository->findByUserId($user->getId()) as $achievementReceived) {
            $achievements[$achievementReceived->getAchievement()->getId()] =
                ReceivedAchievementConverter::convertToDTO($achievementReceived);
        }

        return $achievements;
    }
}
