<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Service;

use App\Domain\Achievement\Service\AchievementFetcherInterface;
use App\Infrastructure\Achievement\Service\AchievementFetcher\AchievementFetcher;
use App\Infrastructure\Achievement\Service\AchievementFetcher\AchievementFetcherForUser;
use LogicException;

/**
 * Class AchievementFetcherFactory.
 */
class AchievementFetcherFactory
{
    public const ACHIEVEMENTS_FOR_USER = 'achievements_for_user';
    public const ACHIEVEMENTS = 'achievements';

    /**
     * @var AchievementFetcherInterface[]
     */
    private $fetchers = [];

    /**
     * AchievementFetcherFactory constructor.
     *
     * @param AchievementFetcher $achievementFetcher
     * @param AchievementFetcherForUser $achievementFetcherForUser
     */
    public function __construct(
        AchievementFetcher $achievementFetcher,
        AchievementFetcherForUser $achievementFetcherForUser
    )
    {
        $this->addFetcher(self::ACHIEVEMENTS, $achievementFetcher);
        $this->addFetcher(self::ACHIEVEMENTS_FOR_USER, $achievementFetcherForUser);
    }

    /**
     * @param string $type
     * @param AchievementFetcherInterface $achievementFetcher
     */
    public function addFetcher(string $type, AchievementFetcherInterface $achievementFetcher)
    {
        $this->fetchers[$type] = $achievementFetcher;
    }

    /**
     * @param string $type
     *
     * @return AchievementFetcherInterface
     */
    public function getFetcher(string $type): AchievementFetcherInterface
    {
        if (!isset($this->fetchers[$type])) {
            throw new LogicException('Invalid achievement fetcher type');
        }

        return $this->fetchers[$type];
    }

}
