<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Service\Processor;

use App\Domain\Achievement\Dictionary\AchievementStrategyType;
use App\Domain\Achievement\Model\AchievementReceivedInterface;
use App\Domain\Achievement\Repository\AchievementReceivedRepositoryInterface;
use App\Domain\Achievement\Repository\AchievementRepositoryInterface;
use App\Domain\Achievement\Service\AchievementProcessorInterface;
use App\Infrastructure\Achievement\Model\UserReceived\ActionAchievementUserReceived;
use DateTime;
use Exception;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class ActiveTypeAchievementProcessor.
 */
class ActiveTypeAchievementProcessor implements AchievementProcessorInterface
{
    /**
     * @var AchievementReceivedRepositoryInterface
     */
    private $achievementReceivedRepository;

    /**
     * @var AchievementRepositoryInterface
     */
    private $achievementRepository;

    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * ActiveTypeAchievementProcessor constructor.
     *
     * @param AchievementReceivedRepositoryInterface $achievementReceivedRepository
     * @param AchievementRepositoryInterface $achievementRepository
     * @param MessageBusInterface $messageBus
     */
    public function __construct(
        AchievementReceivedRepositoryInterface $achievementReceivedRepository,
        AchievementRepositoryInterface $achievementRepository,
        MessageBusInterface $messageBus
    )
    {
        $this->achievementReceivedRepository = $achievementReceivedRepository;
        $this->achievementRepository = $achievementRepository;
        $this->bus = $messageBus;
    }

    /**
     * @param string $achievementName
     * @param string $userId
     *
     * @return AchievementReceivedInterface
     *
     * @throws Exception
     */
    public function process(string $achievementName, string $userId): AchievementReceivedInterface
    {
        $achievement = $this->achievementRepository->findByName($achievementName);

        if ($achievementUserReceived = $this->achievementReceivedRepository->findByAchievementAndUserId($achievement, $userId)) {
            return $achievementUserReceived;
        }

        $achievementUserReceived = new ActionAchievementUserReceived();
        $achievementUserReceived
            ->setAchievement($achievement)
            ->setReceivedAt(new DateTime())
            ->setUserId($userId)
            ->setProgress(1);

        return $this->achievementReceivedRepository->save($achievementUserReceived);
    }

    /**
     * @return string
     */
    public function getProcessorType(): string
    {
        return AchievementStrategyType::ACTION;
    }
}
