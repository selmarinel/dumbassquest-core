<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Service\Processor;

use App\Domain\Achievement\Dictionary\AchievementStrategyType;
use App\Domain\Achievement\Model\AchievementReceivedInterface;
use App\Domain\Achievement\Repository\AchievementReceivedRepositoryInterface;
use App\Domain\Achievement\Repository\AchievementRepositoryInterface;
use App\Domain\Achievement\Service\AchievementProcessorInterface;
use App\Infrastructure\Achievement\Model\UserReceived\ProgressAchievementUserReceived;
use Exception;

/**
 * Class ProgressTypeAchievementProcessor.
 */
class ProgressTypeAchievementProcessor implements AchievementProcessorInterface
{
    /**
     * @var AchievementReceivedRepositoryInterface
     */
    private $achievementReceivedRepository;

    /**
     * @var AchievementRepositoryInterface
     */
    private $achievementRepository;

    /**
     * ProgressTypeAchievementProcessor constructor.
     *
     * @param AchievementReceivedRepositoryInterface $achievementReceivedRepository
     * @param AchievementRepositoryInterface $achievementRepository
     */
    public function __construct(
        AchievementReceivedRepositoryInterface $achievementReceivedRepository,
        AchievementRepositoryInterface $achievementRepository
    )
    {
        $this->achievementReceivedRepository = $achievementReceivedRepository;
        $this->achievementRepository = $achievementRepository;
    }

    /**
     * @return string
     */
    public function getProcessorType(): string
    {
        return AchievementStrategyType::PROGRESS;
    }

    /**
     * @param string $achievementName
     * @param string $userId
     *
     * @return AchievementReceivedInterface
     *
     * @throws Exception
     */
    public function process(string $achievementName, string $userId): AchievementReceivedInterface
    {
        $achievement = $this->achievementRepository->findByName($achievementName);

        /** @var ProgressAchievementUserReceived $achievementReceived */
        $achievementReceived = $this
            ->achievementReceivedRepository
            ->findByAchievementAndUserId($achievement, $userId);

        if (!$achievementReceived) {
            $achievementReceived = new ProgressAchievementUserReceived();
            $achievementReceived
                ->setAchievement($achievement)
                ->setUserId($userId)
                ->setProgress(0)
                ->setStepsAchieved(0);
        }

        if ($achievementReceived->getProgress() >= 1) {
            return $achievementReceived;
        }

        if ($achievementReceived->getStepsAchieved() < $achievementReceived->getAchievement()->getSteps()) {
            $achievementReceived->setStepsAchieved($achievementReceived->getStepsAchieved() + 1);

            $achievementReceived->setProgress($this->calculateProgress($achievementReceived));
        }

        return $this->achievementReceivedRepository->save($achievementReceived);
    }

    /**
     * @param ProgressAchievementUserReceived $achievementUserReceived
     *
     * @return float
     */
    private function calculateProgress(ProgressAchievementUserReceived $achievementUserReceived): float
    {
        if ($achievementUserReceived->getAchievement()->getSteps() <= 0) {
            return 0.0;
        }

        return $achievementUserReceived->getStepsAchieved() / $achievementUserReceived->getAchievement()->getSteps();
    }
}
