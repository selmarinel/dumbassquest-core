<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Service;

use App\Domain\Achievement\Service\AchievementProcessorInterface;
use LogicException;

/**
 * Class AchievementProcessor.
 */
class AchievementProcessorLocator
{
    /**
     * @var AchievementProcessorInterface[]
     */
    private $processors = [];

    /**
     * AchievementProcessorLocator constructor.
     *
     * @param iterable $processors
     */
    public function __construct(iterable $processors)
    {
        foreach ($processors as $processor) {
            $this->addProcessor($processor);
        }
    }

    /**
     * @param string $processorName
     *
     * @return AchievementProcessorInterface
     */
    public function getProcessor(string $processorName): AchievementProcessorInterface
    {
        if (!isset($this->processors[$processorName])) {
            throw new LogicException('Unsupported processor type');
        }

        return $this->processors[$processorName];
    }

    /**
     * @param AchievementProcessorInterface $achievementProcessor
     */
    private function addProcessor(AchievementProcessorInterface $achievementProcessor)
    {
        $this->processors[$achievementProcessor->getProcessorType()] = $achievementProcessor;
    }
}
