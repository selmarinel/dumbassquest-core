<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Service;

use App\Domain\Achievement\DataTransferObject\AchievementDTOInterface;
use App\Domain\Achievement\DataTransferObject\BadgeDTOInterface;
use App\Domain\Achievement\Dictionary\AchievementStrategyType;
use App\Domain\Achievement\Model\AchievementInterface;
use App\Domain\Achievement\Model\BadgeInterface;
use App\Domain\Achievement\Repository\AchievementRepositoryInterface;
use App\Domain\Achievement\Service\AchievementCreatorInterface;
use App\Infrastructure\Achievement\Converter\AchievementConverter;
use App\Infrastructure\Achievement\Model\Achievement;
use App\Infrastructure\Achievement\Model\Badge;
use App\Infrastructure\Achievement\Repository\BadgeRepository;
use LogicException;
use RuntimeException;

/**
 * Class AchievementSaver.
 */
class AchievementSaver implements AchievementCreatorInterface
{
    /**
     * @var AchievementRepositoryInterface
     */
    private $achievementRepository;

    /**
     * @var BadgeRepository
     */
    private $badgeRepository;

    /**
     * AchievementSaver constructor.
     *
     * @param AchievementRepositoryInterface $achievementRepository
     * @param BadgeRepository $badgeRepository
     */
    public function __construct(AchievementRepositoryInterface $achievementRepository, BadgeRepository $badgeRepository)
    {
        $this->achievementRepository = $achievementRepository;
        $this->badgeRepository = $badgeRepository;
    }

    /**
     * @param AchievementDTOInterface $achievementDTO
     *
     * @return AchievementDTOInterface
     */
    public function createAchievement(AchievementDTOInterface $achievementDTO): AchievementDTOInterface
    {
        if ($this->achievementRepository->findByName($achievementDTO->getName())) {
            throw new RuntimeException('achievement.exists');
        }

        switch ($achievementDTO->getType()) {
            case AchievementStrategyType::ACTION:
                $achievement = new Achievement\ActionAchievement();
                break;
            case AchievementStrategyType::PROGRESS:
                $achievement = new Achievement\ProgressAchievement();
                $achievement->setSteps($achievementDTO->getStepsToAchieve());
                break;
            default:
                throw new LogicException('Unsupported achievement type');
        }

        $achievement
            ->setName($achievementDTO->getName())
            ->setDescription($achievementDTO->getDescription());

        $this->achievementRepository->save($achievement);

        return AchievementConverter::convertToDTO($achievement);
    }


    /**
     * @param BadgeDTOInterface $badgeDTO
     *
     * @return BadgeInterface
     */
    public function createBadge(BadgeDTOInterface $badgeDTO): BadgeInterface
    {
        if ($this->badgeRepository->findByName($badgeDTO->getName())) {
            throw new RuntimeException('badge.exists');
        }

        $badge = new Badge();
        $badge
            ->setName($badgeDTO->getName())
            ->setDescription($badgeDTO->getDescription());

        return $this->badgeRepository->save($badge);
    }

    /**
     * @param AchievementDTOInterface $achievement
     * @param BadgeDTOInterface $badge
     *
     * @return BadgeInterface
     */
    public function attachAchievementToBadge(AchievementDTOInterface $achievement, BadgeDTOInterface $badge): BadgeInterface
    {
        if (!$achievement = $this->achievementRepository->findByName($achievement->getName())) {
            throw new RuntimeException('achievement.not.found');
        }  //to validation

        if (!$badge = $this->badgeRepository->findByName($badge->getName())) {
            throw new RuntimeException('badge.not.found');
        } //to validation

        if (!$this->hasBadgeAchievement($badge, $achievement)) {
            $badge->addAchievement($achievement);
            $this->badgeRepository->save($badge);
        }

        return $badge;
    }

    /**
     * @param BadgeInterface $badge
     * @param AchievementInterface $achievement
     *
     * @return bool
     * @todo move to another
     *
     * @deprecated
     */
    private function hasBadgeAchievement(BadgeInterface $badge, AchievementInterface $achievement): bool
    {
        foreach ($badge->getAchievements() as $achievementInBadge) {
            if ($achievementInBadge->getName() === $achievement->getName()) {
                return true;
            }
        }

        return false;
    }
}
