<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Service;

use App\Domain\Achievement\Repository\AchievementRepositoryInterface;
use App\Domain\Achievement\Service\AchievementObtainInterface;
use RuntimeException;

/**
 * Class AchievementObtainHandler.
 */
class AchievementObtainHandler implements AchievementObtainInterface
{
    /**
     * @var AchievementProcessorLocator
     */
    private $achievementProcessorLocator;
    /**
     * @var AchievementRepositoryInterface
     */
    private $achievementRepository;

    /**
     * AchievementGrand constructor.
     *
     * @param AchievementRepositoryInterface $achievementRepository
     * @param AchievementProcessorLocator $achievementProcessorLocator
     */
    public function __construct(
        AchievementRepositoryInterface $achievementRepository,
        AchievementProcessorLocator $achievementProcessorLocator
    )
    {
        $this->achievementRepository = $achievementRepository;
        $this->achievementProcessorLocator = $achievementProcessorLocator;
    }

    /**
     * @param string $achievementName
     * @param string $userId
     */
    public function obtain(string $achievementName, ?string $userId): void
    {
        if (!$userId) {
            throw new RuntimeException('user not auth');
        }
        $achievement = $this->achievementRepository->findByName($achievementName);
        if (!$achievement) {
            throw new RuntimeException('achievement.not_found');
        }
        $processor = $this->achievementProcessorLocator->getProcessor($achievement->getType());
        $processor->process($achievement->getName(), $userId);
    }
}
