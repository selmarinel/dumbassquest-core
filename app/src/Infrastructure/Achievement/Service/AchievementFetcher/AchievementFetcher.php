<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Service\AchievementFetcher;

use App\Domain\Achievement\DataTransferObject\AchievementDTOInterface;
use App\Domain\Achievement\DataTransferObject\BadgeDTOInterface;
use App\Domain\Achievement\Repository\AchievementRepositoryInterface;
use App\Domain\Achievement\Service\AchievementFetcherInterface;
use App\Infrastructure\Achievement\Converter\AchievementConverter;
use App\Infrastructure\Achievement\DataTransferObject\BadgeDTO;
use App\Infrastructure\Achievement\Repository\BadgeRepository;
use RuntimeException;

/**
 * Class AchievementFetcher.
 */
class AchievementFetcher implements AchievementFetcherInterface
{
    /**
     * @var AchievementRepositoryInterface
     */
    private $achievementRepository;

    /**
     * @var BadgeRepository
     */
    private $badgeRepository;

    /**
     * AchievementFetcher constructor.
     *
     * @param AchievementRepositoryInterface $achievementRepository
     * @param BadgeRepository $badgeRepository
     */
    public function __construct(AchievementRepositoryInterface $achievementRepository, BadgeRepository $badgeRepository)
    {
        $this->achievementRepository = $achievementRepository;
        $this->badgeRepository = $badgeRepository;
    }

    /**
     * @param string $name
     *
     * @return AchievementDTOInterface
     */
    public function fetchAchievement(string $name): AchievementDTOInterface
    {
        if (!$achievement = $this->achievementRepository->findByName($name)) {
            throw new RuntimeException('achievement.not.found');
        }

        return AchievementConverter::convertToDTO($achievement);
    }

    /**
     * @param string $name
     *
     * @return BadgeDTOInterface
     */
    public function fetchBadge(string $name): BadgeDTOInterface
    {
        if (!$badge = $this->badgeRepository->findByName($name)) {
            throw new RuntimeException('badge.not.found');
        }

        $badgeDTO = new BadgeDTO();
        $badgeDTO
            ->setName($badge->getName())
            ->setDescription($badge->getDescription());

        foreach ($badge->getAchievements() as $achievement) {
            $badgeDTO->addAchievement(AchievementConverter::convertToDTO($achievement));
        }

        return $badgeDTO;
    }

    /**
     * @return AchievementDTOInterface[]
     */
    public function fetchAchievements(): array
    {
        $result = [];
        foreach ($this->achievementRepository->all() as $achievement) {
            $result[] = AchievementConverter::convertToDTO($achievement);
        }

        return $result;
    }
}
