<?php

declare(strict_types=1);

namespace App\Infrastructure\Achievement\Service\AchievementFetcher;

use App\Domain\Achievement\DataTransferObject\AchievementDTOInterface;
use App\Domain\Achievement\DataTransferObject\BadgeDTOInterface;
use App\Domain\Achievement\Service\AchievementFetcherInterface;
use App\Infrastructure\Achievement\Misc\TokenUserTrait;
use App\Infrastructure\Achievement\Service\ReceivedFetcher\UserReceivedAchievementFetcher;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class AchievementFetcherForUser.
 */
class AchievementFetcherForUser implements AchievementFetcherInterface
{
    use TokenUserTrait;
    /**
     * @var AchievementFetcherInterface
     */
    private $achievementFetcher;

    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @var UserReceivedAchievementFetcher
     */
    private $userReceivedAchievementFetcher;

    /**
     * AchievementFetcherForUser constructor.
     *
     * @param AchievementFetcher $achievementFetcher
     * @param UserReceivedAchievementFetcher $userReceivedAchievementFetcher
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        AchievementFetcher $achievementFetcher,
        UserReceivedAchievementFetcher $userReceivedAchievementFetcher,
        TokenStorageInterface $tokenStorage
    )
    {
        $this->achievementFetcher = $achievementFetcher;
        $this->userReceivedAchievementFetcher = $userReceivedAchievementFetcher;
        $this->token = $tokenStorage;
    }

    /**
     * @return AchievementDTOInterface[]
     */
    public function fetchAchievements(): array
    {
        $allAchievements = $this->achievementFetcher->fetchAchievements();

        $receivedAchievementsForUser = $this->userReceivedAchievementFetcher->fetchReceivedAchievementsForUser($this->getUser());
        $result = [];

        /** @var AchievementDTOInterface $achievement */
        foreach ($allAchievements as $achievement) {
            if (key_exists($achievement->getId(), $receivedAchievementsForUser)) {
                $result[] = $receivedAchievementsForUser[$achievement->getId()];
                continue;
            }

            $result[] = $achievement;
        }

        return $result;
    }

    /**
     * @param string $name
     *
     * @return AchievementDTOInterface
     */
    public function fetchAchievement(string $name): AchievementDTOInterface
    {
        return $this->achievementFetcher->fetchAchievement($name);
    }

    /**
     * @param string $name
     *
     * @return BadgeDTOInterface
     */
    public function fetchBadge(string $name): BadgeDTOInterface
    {
        return $this->achievementFetcher->fetchBadge($name);
    }
}
