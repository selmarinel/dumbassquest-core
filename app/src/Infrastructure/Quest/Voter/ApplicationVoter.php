<?php

declare(strict_types=1);

namespace App\Infrastructure\Quest\Voter;

use App\Domain\Person\Model\User\GetUserInterface;
use App\Infrastructure\Quest\Model\Application;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ApplicationVoter extends Voter
{
    public const CAN_TRANSIT_APPLICATION = 'can_transit_application';

    /**
     * @inheritDoc
     */
    protected function supports($attribute, $subject): bool
    {
        if (!$subject instanceof Application) {
            return false;
        }

        if ($attribute !== self::CAN_TRANSIT_APPLICATION) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param Application $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof GetUserInterface) {
            return false;
        }

        return $subject->getUserId() === $user->getId();
    }
}
