<?php

namespace App\Infrastructure\Quest\Repository;

use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Model\Quest\SetQuestInterface;
use App\Domain\Quest\Repository\QuestRepositoryInterface;
use Doctrine\ODM\MongoDB\LockException;
use Doctrine\ODM\MongoDB\Mapping\MappingException;
use Doctrine\ODM\MongoDB\Query\Builder;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

/**
 * Class QuestRepository
 */
class QuestRepository extends DocumentRepository implements QuestRepositoryInterface
{
    /**
     * @return Builder
     */
    public function getBuilder(): Builder
    {
        return $this->createQueryBuilder();
    }

    /**
     * @param SetQuestInterface|GetQuestInterface $quest
     *
     * @return GetQuestInterface
     */
    public function save(SetQuestInterface $quest): GetQuestInterface
    {
        $this->dm->persist($quest);
        $this->dm->flush($quest);

        return $quest;
    }

    /**
     * @param string $id
     *
     * @return GetQuestInterface|null
     *
     * @throws LockException
     * @throws MappingException
     */
    public function findById(string $id): ?GetQuestInterface
    {
        /** @var GetQuestInterface $quest */
        $quest = $this->find($id);

        return $quest;
    }
}