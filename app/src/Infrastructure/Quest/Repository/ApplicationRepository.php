<?php

namespace App\Infrastructure\Quest\Repository;

use App\Domain\Quest\Dictionary\ApplicationStatus;
use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Domain\Quest\Model\Application\SetApplicationInterface;
use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Repository\ApplicationRepositoryInterface;
use Doctrine\ODM\MongoDB\Query\Builder;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;
use MongoException;
use MongoId;

/**
 * Class QuestCommitRepository
 */
class ApplicationRepository extends DocumentRepository implements ApplicationRepositoryInterface
{
    /**
     * @param SetApplicationInterface|GetApplicationInterface $questCommit
     *
     * @return GetApplicationInterface
     */
    public function save(SetApplicationInterface $questCommit): GetApplicationInterface
    {
        $this->dm->persist($questCommit);
        $this->dm->flush($questCommit);

        $this->dm->clear($questCommit);

        return $questCommit;
    }

    /**
     * @param string $questCommitId
     *
     * @return GetApplicationInterface|null
     */
    public function findById(string $questCommitId): ?GetApplicationInterface
    {
        /**
         * @var GetApplicationInterface $questCommit
         */
        $questCommit = $this->findOneBy(['id' => $questCommitId]);

        return $questCommit;
    }

    /**
     * @param string $userId
     *
     * @return GetApplicationInterface[]
     * @deprecated
     *
     * @todo make with filter DTO
     *
     */
    public function findByUserId(string $userId): array
    {
        return $this->findBy(['userId' => $userId]);
    }

    /**
     * @return Builder
     */
    public function getBuilder(): Builder
    {
        return $this->createQueryBuilder();
    }

    /**
     * @param GetQuestInterface $quest
     * @param string $userId
     *
     * @return GetApplicationInterface|null
     *
     * @throws MongoException
     */
    public function findByQuestIdAndUserId(GetQuestInterface $quest, string $userId): ?GetApplicationInterface
    {
        /** @var GetApplicationInterface $questCommit */
        $questCommit = $this->createQueryBuilder()
            ->field('quest')->equals(new MongoId($quest->getId()))
            ->field('userId')->equals($userId)
            ->field('status')->notIn([
                ApplicationStatus::DECLINED,
                ApplicationStatus::COMPLETED,
                ApplicationStatus::FAILED,
            ])
            ->limit(1)
            ->getQuery()
            ->getSingleResult();

        return $questCommit;
    }

    /**
     * @inheritDoc
     */
    public function findByUserIdAndQuests(string $userId, array $questIds): array
    {
        return $this->createQueryBuilder()
            ->field('userId')->equals($userId)
            ->field('quest')->in(array_map(static function ($questId) {
                return new MongoId($questId);
            }, $questIds))
            ->field('status')->notIn([
                ApplicationStatus::DECLINED,
//                ApplicationStatus::COMPLETED,
                ApplicationStatus::FAILED,
            ])
            ->getQuery()
            ->toArray();
    }
}