<?php

namespace App\Infrastructure\Quest\Model;

use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Model\Quest\SetQuestInterface;
use App\Infrastructure\Quest\Repository\QuestRepository;
use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class Quest
 * @ODM\Document(repositoryClass=QuestRepository::class)
 */
class Quest implements GetQuestInterface, SetQuestInterface
{
    /**
     * @ODM\Id()
     */
    private ?string $id = null;

    /**
     * @ODM\Field(type="string")
     */
    private string $userId;

    /**
     * @ODM\Field(type="string")
     */
    private string $title;

    /**
     * @ODM\Field(type="string")
     */
    private string $description;

    /**
     * @ODM\Field(type="string")
     */
    private string $type;

    /**
     * @ODM\Field(type="string")
     */
    private string $status;

    /**
     * @ODM\Field(type="date")
     */
    private DateTime $created;

    /**
     * @ODM\Field(type="date")
     */
    private ?DateTime $expirationDate;

    /**
     * @ODM\Field(type="string")
     */
    private ?string $icon = null;

    /**
     * @ODM\Field(type="boolean")
     */
    private bool $private = false;

    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     *
     * @return self
     */
    public function setUserId(string $userId): SetQuestInterface
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return SetQuestInterface
     */
    public function setTitle(string $title): SetQuestInterface
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return SetQuestInterface
     */
    public function setDescription(string $description): SetQuestInterface
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return SetQuestInterface
     */
    public function setType(string $type): SetQuestInterface
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus(string $status): SetQuestInterface
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     *
     * @return SetQuestInterface
     */
    public function setCreated(DateTime $created): SetQuestInterface
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getExpirationDate(): ?DateTime
    {
        return $this->expirationDate;
    }

    /**
     * @param DateTime|null $expirationDate
     *
     * @return SetQuestInterface
     */
    public function setExpirationDate(?DateTime $expirationDate): SetQuestInterface
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string|null $icon
     *
     * @return self
     */
    public function setIcon(string $icon): SetQuestInterface
    {
        $this->icon = $icon;

        return $this;
    }

    public function isPrivate(): bool
    {
        return $this->private;
    }

    public function setPrivate(bool $private): SetQuestInterface
    {
        $this->private = $private;

        return $this;
    }
}