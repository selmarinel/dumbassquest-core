<?php

namespace App\Infrastructure\Quest\Model;

use App\Domain\Quest\Dictionary\ApplicationStatus;
use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Domain\Quest\Model\Application\SetApplicationInterface;
use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Infrastructure\Quest\Repository\ApplicationRepository;
use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class Application
 *
 * @ODM\Document(repositoryClass=ApplicationRepository::class, collection="applications")
 */
class Application implements SetApplicationInterface, GetApplicationInterface
{
    /**
     * @ODM\Id()
     *
     * @var string
     */
    private $id;

    /**
     * @ODM\Field(type="string")
     *
     * @var string
     */
    private $userId;

    /**
     * @ODM\ReferenceOne(targetDocument="Quest", storeAs="id")
     *
     * @var Quest
     */
    private $quest;

    /**
     * @ODM\Field(type="date")
     *
     * @var DateTime
     */
    private $acceptedAt;

    /**
     * @ODM\Field(type="string")
     *
     * @var string
     */
    private $status = ApplicationStatus::ACCEPTED;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     *
     * @return SetApplicationInterface
     */
    public function setUserId(string $userId): SetApplicationInterface
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return Quest|GetQuestInterface
     */
    public function getQuest(): GetQuestInterface
    {
        return $this->quest;
    }

    /**
     * @param GetQuestInterface|Quest $quest
     *
     * @return SetApplicationInterface
     */
    public function setQuest(GetQuestInterface $quest): SetApplicationInterface
    {
        $this->quest = $quest;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getAcceptedAt(): DateTime
    {
        return $this->acceptedAt;
    }

    /**
     * @param DateTime $acceptedAt
     *
     * @return SetApplicationInterface
     */
    public function setAcceptedAt(DateTime $acceptedAt): SetApplicationInterface
    {
        $this->acceptedAt = $acceptedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}