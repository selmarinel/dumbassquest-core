<?php

namespace App\Infrastructure\Quest\Security;

use App\Domain\Person\Model\User\GetUserInterface;
use App\Domain\Quest\Dictionary\QuestStatus;
use App\Domain\Quest\Model\Quest\GetQuestInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class QuestOwnerVoter
 */
class QuestVoter extends Voter
{
    public const CAN_GET_QUEST = 'CAN_GET_QUEST';

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        //todo make more if need or attribute equal vote constant
        if ($attribute !== self::CAN_GET_QUEST) {
            return false;
        }

        if (!$subject instanceof GetQuestInterface) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param GetQuestInterface $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case self::CAN_GET_QUEST:
                if ($subject->getStatus() === QuestStatus::STATUS_ACTIVE) {
                    return true;
                }

                if ($user = $this->getUser($token)) {
                    return $subject->getUserId() === $user->getId();
                }

                break;
        }

        return false;
    }

    /**
     * @param TokenInterface $token
     *
     * @return GetUserInterface|null
     */
    private function getUser(TokenInterface $token): ?GetUserInterface
    {
        $user = $token->getUser();

        return ($user instanceof GetUserInterface) ? $user : null;
    }
}