<?php

namespace App\Infrastructure\Quest\Security;

use App\Domain\Person\Model\User\GetUserInterface;
use App\Domain\Quest\Service\QuestFetcher;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class QuestOwnerVoter
 * @package App\Infrastructure\Quest\Security
 */
class QuestOwnerVoter extends Voter
{
    public const IS_QUEST_OWNER = 'IS_QUEST_OWNER';

    /**
     * @var QuestFetcher
     */
    private $questFetcher;

    /**
     * QuestOwnerVoter constructor.
     * @param QuestFetcher $questFetcher
     */
    public function __construct(QuestFetcher $questFetcher)
    {
        $this->questFetcher = $questFetcher;
    }

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        if ($attribute !== self::IS_QUEST_OWNER) {
            return false;
        }

        if (!is_string($subject)) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof GetUserInterface) {
            return false;
        }

        if (!$quest = $this->questFetcher->fetchQuest($subject)){
            return false;
        }

        switch ($attribute) {
            case self::IS_QUEST_OWNER:
                return $quest->getUserId() === $user->getId();
                break;
        }

        return false;
    }
}