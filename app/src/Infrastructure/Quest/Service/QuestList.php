<?php

namespace App\Infrastructure\Quest\Service;

use App\Domain\Quest\Dictionary\QuestStatus;
use App\Domain\Quest\Misc\UserStorageInterface;
use App\Domain\Quest\Repository\QuestRepositoryInterface;
use App\Domain\Quest\Service\QuestListInterface;
use App\Domain\Quest\ValueObject\QuestListFilterVOInterface;
use App\Domain\Quest\ValueObject\QuestListVOInterface;
use App\Infrastructure\Quest\Repository\QuestRepository;
use App\Infrastructure\Quest\ValueObject\QuestListVO;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ODM\MongoDB\Query\Builder;
use MongoException;
use MongoRegex;

/**
 * Class QuestList
 */
class QuestList implements QuestListInterface
{
    private QuestRepositoryInterface $questRepository;
    private UserStorageInterface $userStorage;

    public function __construct(QuestRepository $questRepository, UserStorageInterface $userStorage)
    {
        $this->questRepository = $questRepository;
        $this->userStorage = $userStorage;
    }

    /**
     * @param QuestListFilterVOInterface $questListFilterVO
     *
     * @return QuestListVOInterface
     *
     * @throws MongoException
     */
    public function getList(QuestListFilterVOInterface $questListFilterVO): QuestListVOInterface
    {
        $builder = $this->prepareBuilder($questListFilterVO)
            ->sort('created', -1)
            ->limit($questListFilterVO->getTake())
            ->skip($questListFilterVO->getOffset());

        $list = new QuestListVO();

        return $list
            ->setItems($builder->getQuery()->toArray())
            ->setTotal($this->getTotal($questListFilterVO));
    }

    /**
     * @param QuestListFilterVOInterface $questListFilterVO
     *
     * @return int
     */
    private function getTotal(QuestListFilterVOInterface $questListFilterVO): int
    {
        try {
            return $this
                ->prepareBuilder($questListFilterVO)
                ->getQuery()
                ->execute()
                ->count();
        } catch (MongoDBException $exception) {
            return 0;
        } catch (MongoException $exception) {
            return 0;
        }
    }

    /**
     * @param QuestListFilterVOInterface $questListFilterVO
     *
     * @return Builder
     *
     * @throws MongoException
     */
    private function prepareBuilder(QuestListFilterVOInterface $questListFilterVO): Builder
    {
        $isDeletedStatus = $questListFilterVO->isDeleted() ? QuestStatus::STATUS_DELETED : QuestStatus::STATUS_ACTIVE;

        $builder = $this->questRepository->getBuilder()
            ->field('status')->equals($isDeletedStatus);

        if ($questListFilterVO->getQuery()) {
            $builder->field('title')->equals(new MongoRegex('/' . $questListFilterVO->getQuery() . '/'));
        }

        if ($questListFilterVO->getType()) {
            $builder->field('type')->in($questListFilterVO->getType());
        }

        $user = null;
        try {
            $user = $this->userStorage->getUser();
        } catch (\Throwable $exception) {

        }

        if (null !== $user) {
            $builder->addOr(
                $builder->expr()->field('private')->equals(false),
                $builder->expr()->field('private')->exists(false),
                $builder->expr()->addAnd(
                    $builder->expr()->field('private')->equals(true),
                    $builder->expr()->field('userId')->equals($user->getId())
                )
            );
        } else {
            $builder->addOr(
                $builder->expr()->field('private')->equals(false),
                $builder->expr()->field('private')->exists(false)
            );
        }

        if (!empty($questListFilterVO->getUsers())) {
            $builder->field('userId')->in($questListFilterVO->getUsers());
        }

        //todo add more filters for example user, location

        return $builder;
    }
}