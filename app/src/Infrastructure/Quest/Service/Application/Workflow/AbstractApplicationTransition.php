<?php

declare(strict_types=1);

namespace App\Infrastructure\Quest\Service\Application\Workflow;

use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Domain\Quest\Model\Application\SetApplicationInterface;
use App\Domain\Quest\Repository\ApplicationRepositoryInterface;
use App\Domain\Quest\Service\Application\Workflow\ApplicationTransitionInterface;
use Symfony\Component\Workflow\WorkflowInterface;

/**
 * Class AbstractApplicationTransition.
 *
 * @mixin ApplicationTransitionInterface
 */
abstract class AbstractApplicationTransition
{
    protected WorkflowInterface $applicationStateMachine;

    protected ApplicationRepositoryInterface $applicationRepository;

    public function __construct(
        WorkflowInterface $applicationStateMachine,
        ApplicationRepositoryInterface $applicationRepository
    )
    {
        $this->applicationStateMachine = $applicationStateMachine;
        $this->applicationRepository = $applicationRepository;
    }

    /**
     * @param SetApplicationInterface $application
     *
     * @return GetApplicationInterface
     */
    public function transit(SetApplicationInterface $application): GetApplicationInterface
    {
        $this->applicationStateMachine->apply($application, $this->getTransitionName());

        return $this->applicationRepository->save($application);
    }

    /**
     * @return string
     */
    abstract protected function getTransitionName(): string;
}
