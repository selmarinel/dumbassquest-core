<?php

declare(strict_types=1);

namespace App\Infrastructure\Quest\Service\Application\Workflow;

use App\Domain\Quest\Dictionary\ApplicationTransition;
use App\Domain\Quest\Misc\UserStorageInterface;
use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Domain\Quest\Model\Application\SetApplicationInterface;
use App\Domain\Quest\Repository\ApplicationRepositoryInterface;
use App\Domain\Quest\Service\Application\Workflow\ApplicationTransitionInterface;
use App\Infrastructure\Person\Event\GrandExperienceEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Workflow\WorkflowInterface;

/**
 * Class ApplicationCompleteTransition.
 */
class ApplicationCompleteTransition extends AbstractApplicationTransition implements ApplicationTransitionInterface
{
    private EventDispatcherInterface $dispatcher;
    private UserStorageInterface $userStorage;

    public function __construct(
        UserStorageInterface $userStorage,
        EventDispatcherInterface $eventDispatcher,
        WorkflowInterface $applicationStateMachine,
        ApplicationRepositoryInterface $applicationRepository
    )
    {
        $this->userStorage = $userStorage;
        $this->dispatcher = $eventDispatcher;
        parent::__construct($applicationStateMachine, $applicationRepository);
    }

    /**
     * @inheritDoc
     */
    public function getTransitionName(): string
    {
        return ApplicationTransition::COMPLETE;
    }

    public function transit(SetApplicationInterface $application): GetApplicationInterface
    {
        $application = parent::transit($application);

        $this->dispatcher->dispatch(new GrandExperienceEvent($this->userStorage->getUser(), 1));

        return $application;
    }
}
