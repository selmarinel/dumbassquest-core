<?php

declare(strict_types=1);

namespace App\Infrastructure\Quest\Service\Application\Workflow;

use App\Domain\Quest\Dictionary\ApplicationTransition;
use App\Domain\Quest\Service\Application\Workflow\ApplicationTransitionInterface;

/**
 * Class ApplicationDeclineTransition.
 */
class ApplicationDeclineTransition extends AbstractApplicationTransition implements ApplicationTransitionInterface
{
    /**
     * @inheritDoc
     */
    public function getTransitionName(): string
    {
        return ApplicationTransition::DECLINE;
    }
}
