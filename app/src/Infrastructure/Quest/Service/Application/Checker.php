<?php

declare(strict_types=1);

namespace App\Infrastructure\Quest\Service\Application;

use App\Domain\Quest\Misc\UserStorageInterface;
use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Repository\ApplicationRepositoryInterface;
use App\Domain\Quest\Service\Checker\CheckInterface;

/**
 * Class Checker.
 */
class Checker implements CheckInterface
{
    /**
     * @var ApplicationRepositoryInterface
     */
    private $repository;

    /**
     * @var UserStorageInterface
     */
    private $userStorage;

    /**
     * Checker constructor.
     *
     * @param ApplicationRepositoryInterface $repository
     * @param UserStorageInterface $userStorage
     */
    public function __construct(ApplicationRepositoryInterface $repository, UserStorageInterface $userStorage)
    {
        $this->repository = $repository;
        $this->userStorage = $userStorage;
    }

    /**
     * @inheritDoc
     */
    public function check(GetQuestInterface $quest): bool
    {
        if ($questCommit = $this->repository->findByQuestIdAndUserId($quest, $this->userStorage->getUserId())) {
            return false;
        }

        return true;
    }
}
