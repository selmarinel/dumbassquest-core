<?php

declare(strict_types=1);

namespace App\Infrastructure\Quest\Service\Application;

use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Domain\Quest\Model\Application\SetApplicationInterface;
use App\Domain\Quest\Repository\ApplicationRepositoryInterface;
use App\Domain\Quest\Service\Application\Workflow\ApplicationStatusChangerInterface;
use Symfony\Component\Workflow\WorkflowInterface;

/**
 * Class ApplicationStatusChanger.
 */
class ApplicationStatusChanger implements ApplicationStatusChangerInterface
{
    private ApplicationRepositoryInterface $applicationRepository;
    private WorkflowInterface $applicationWorkflow;

    public function __construct(
        WorkflowInterface $applicationWorkflow,
        ApplicationRepositoryInterface $applicationRepository
    )
    {
        $this->applicationWorkflow = $applicationWorkflow;
        $this->applicationRepository = $applicationRepository;
    }

    /**
     * @param string $action
     * @param SetApplicationInterface $application
     *
     * @return GetApplicationInterface
     */
    public function transit(string $action, SetApplicationInterface $application): GetApplicationInterface
    {
        $this->applicationWorkflow->apply($application, $action);

        return $this->applicationRepository->save($application);
    }
}
