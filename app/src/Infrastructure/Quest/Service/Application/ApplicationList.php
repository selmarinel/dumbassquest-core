<?php

namespace App\Infrastructure\Quest\Service\Application;

use App\Domain\Quest\Repository\ApplicationRepositoryInterface;
use App\Domain\Quest\Service\Application\ApplicationListInterface;
use App\Domain\Quest\ValueObject\QuestCommit\ApplicationListFilterVOInterface;
use App\Domain\Quest\ValueObject\QuestCommit\ApplicationListVOInterface;
use App\Infrastructure\Quest\Repository\ApplicationRepository;
use App\Infrastructure\Quest\ValueObject\Application\ApplicationListVO;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ODM\MongoDB\Query\Builder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class QuestCommitList
 */
class ApplicationList implements ApplicationListInterface
{
    /**
     * @var ApplicationRepositoryInterface|ApplicationRepository
     */
    private $questCommitRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * QuestCommitList constructor.
     * @param ApplicationRepositoryInterface $questCommitRepository
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ApplicationRepositoryInterface $questCommitRepository, TokenStorageInterface $tokenStorage)
    {
        $this->questCommitRepository = $questCommitRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param ApplicationListFilterVOInterface $filterVO
     *
     * @return ApplicationListVOInterface
     */
    public function getList(ApplicationListFilterVOInterface $filterVO): ApplicationListVOInterface
    {
        $items = $this->prepareBuilder($filterVO)
            ->sort('acceptedAt', 'desc')
            ->skip($filterVO->getOffset())
            ->limit($filterVO->getTake())
            ->getQuery()
            ->toArray();

        $listVO = new ApplicationListVO();
        $listVO
            ->setItems($items)
            ->setTotal($this->getTotal($filterVO));

        return $listVO;
    }

    /**
     * @param ApplicationListFilterVOInterface $filterVO
     *
     * @return Builder
     */
    private function prepareBuilder(ApplicationListFilterVOInterface $filterVO): Builder
    {
        $builder = $this->questCommitRepository
            ->getBuilder()
            ->field('userId')->equals($filterVO->getUserId());

        if ($filterVO->getStatus()) {
            $builder->field('status')->equals($filterVO->getStatus());
        }

        return $builder;
    }

    /**
     * @param ApplicationListFilterVOInterface $filterVO
     *
     * @return int
     */
    private function getTotal(ApplicationListFilterVOInterface $filterVO): int
    {
        try {
            return $this->prepareBuilder($filterVO)
                ->getQuery()
                ->execute()
                ->count();
        } catch (MongoDBException $exception) {
            return 0;
        }
    }
}