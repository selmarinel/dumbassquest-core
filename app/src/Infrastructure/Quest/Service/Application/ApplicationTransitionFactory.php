<?php

declare(strict_types=1);

namespace App\Infrastructure\Quest\Service\Application;

use App\Domain\Quest\Service\Application\Workflow\ApplicationTransitionInterface;
use LogicException;

/**
 * Class ApplicationTransitionFactory.
 */
class ApplicationTransitionFactory
{
    /**
     * @var ApplicationTransitionInterface[]
     */
    private array $transitions = [];

    public function __construct(iterable $transitions)
    {
        foreach ($transitions as $transition) {
            if ($transition instanceof ApplicationTransitionInterface) {
                $this->addTransition($transition);
            }
        }
    }

    /**
     * @param string $transition
     *
     * @return ApplicationTransitionInterface
     */
    public function getTransition(string $transition): ApplicationTransitionInterface
    {

        if (!isset($this->transitions[$transition])) {
            throw new LogicException('unsupported transition');
        }

        return $this->transitions[$transition];
    }

    /**
     * @param ApplicationTransitionInterface $applicationTransition
     */
    public function addTransition(ApplicationTransitionInterface $applicationTransition): void
    {
        $this->transitions[$applicationTransition->getTransitionName()] = $applicationTransition;
    }
}
