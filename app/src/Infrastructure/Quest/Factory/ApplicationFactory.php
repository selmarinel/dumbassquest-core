<?php

declare(strict_types=1);

namespace App\Infrastructure\Quest\Factory;

use App\Domain\Quest\Factory\ApplicationFactoryInterface;
use App\Domain\Quest\Misc\UserStorageInterface;
use App\Domain\Quest\Model\Application\SetApplicationInterface;
use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Infrastructure\Quest\Model\Application;
use DateTime;

/**
 * Class QuestCommitFactory.
 */
class ApplicationFactory implements ApplicationFactoryInterface
{
    /**
     * @var UserStorageInterface
     */
    private $userStorage;

    /**
     * QuestCommitFactory constructor.
     *
     * @param UserStorageInterface $userStorage
     */
    public function __construct(UserStorageInterface $userStorage)
    {
        $this->userStorage = $userStorage;
    }

    /**
     * @inheritDoc
     */
    public function createNewQuestApplication(GetQuestInterface $quest): SetApplicationInterface
    {
        $questCommit = new Application();
        $questCommit
            ->setUserId($this->userStorage->getUserId())
            ->setQuest($quest)
            ->setAcceptedAt(new DateTime());

        return $questCommit;
    }
}
