<?php

declare(strict_types=1);

namespace App\Infrastructure\Quest\Factory;

use App\Domain\Quest\Dictionary\QuestStatus;
use App\Domain\Quest\Dictionary\QuestType;
use App\Domain\Quest\Factory\QuestFactoryInterface;
use App\Domain\Quest\Misc\UserStorageInterface;
use App\Domain\Quest\Model\Quest\SetQuestInterface;
use App\Domain\Quest\ValueObject\QuestVOInterface;
use App\Infrastructure\Quest\Model\Quest;
use DateTime;

/**
 * Class QuestFactory.
 */
class QuestFactory implements QuestFactoryInterface
{
    /**
     * @var UserStorageInterface
     */
    private $userStorage;

    /**
     * QuestFactory constructor.
     *
     * @param UserStorageInterface $userStorage
     */
    public function __construct(UserStorageInterface $userStorage)
    {
        $this->userStorage = $userStorage;
    }

    /**
     * @inheritDoc
     */
    public function createQuest(QuestVOInterface $questVO): SetQuestInterface
    {
        $quest = new Quest();
        $quest
            ->setTitle($questVO->getTitle())
            ->setDescription($questVO->getDescription())
            ->setExpirationDate($questVO->getExpirationDate())
            ->setIcon($questVO->getIcon())
            ->setStatus(QuestStatus::STATUS_ACTIVE)
            ->setType(QuestType::SYSTEM_TYPE_NEW)
            ->setCreated(new DateTime())
            ->setPrivate($questVO->isPrivate())
            ->setUserId($this->userStorage->getUserId());

        return $quest;
    }
}
