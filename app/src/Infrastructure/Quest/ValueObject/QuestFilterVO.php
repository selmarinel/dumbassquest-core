<?php

namespace App\Infrastructure\Quest\ValueObject;

use App\Domain\Quest\ValueObject\QuestListFilterVOInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class QuestFilterVO
 */
class QuestFilterVO implements QuestListFilterVOInterface
{
    /**
     * @Serializer\Type("string")
     */
    private ?string $query = null;

    /**
     * @Serializer\Type("int")
     */
    private int $take = 10;

    /**
     * @Serializer\Type("int")
     */
    private int $page = 1;

    /**
     * @Serializer\Type("array")
     *
     * @var string[]
     */
    private array $type = [];

    /**
     * @Serializer\Type("array")
     *
     * @var array
     */
    private array $users = [];

    /**
     * @Serializer\Type("bool")
     */
    private bool $isDeleted = false;

    /**
     * @return int
     */
    public function getTake(): int
    {
        return $this->take;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        $page = ($this->page > 1) ? $this->page : 1;

        return ($page - 1) * $this->take;
    }

    /**
     * @param int $take
     *
     * @return QuestListFilterVOInterface
     */
    public function setTake(int $take): QuestListFilterVOInterface
    {
        $this->take = $take;

        return $this;
    }

    /**
     * @param int $page
     *
     * @return QuestListFilterVOInterface
     */
    public function setPage(int $page): QuestListFilterVOInterface
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $deleted
     *
     * @return QuestListFilterVOInterface
     */
    public function setIsDeleted(bool $deleted): QuestListFilterVOInterface
    {
        $this->isDeleted = $deleted;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getQuery(): ?string
    {
        return $this->query;
    }

    /**
     * @param string|null $query
     *
     * @return self
     */
    public function setQuery(?string $query): self
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getType(): array
    {
        return $this->type;
    }

    /**
     * @param string[] $type
     *
     * @return self
     */
    public function setType(array $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return array
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * @param array $users
     *
     * @return self
     */
    public function setUsers(array $users): self
    {
        $this->users = $users;

        return $this;
    }
}