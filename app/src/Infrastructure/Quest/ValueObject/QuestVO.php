<?php

namespace App\Infrastructure\Quest\ValueObject;

use App\Domain\Quest\ValueObject\QuestVOInterface;
use DateTime;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class QuestVO implements QuestVOInterface
{
    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"update"})
     */
    private ?string $id = null;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"create", "update"})
     */
    private string $title;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"create", "update"})
     */
    private string $description;

    /**
     * @Assert\DateTime()
     *
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     * @Serializer\Groups({"create", "update"})
     */
    private ?DateTime $expirationDate = null;

    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"create", "update"})
     */
    private ?string $icon;

    /**
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"create", "update"})
     */
    private bool $private = false;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function setExpirationDate(?DateTime $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getExpirationDate(): ?DateTime
    {
        return $this->expirationDate;
    }

    /**
     * todo return Location type
     * todo add to doctrine location type
     * add to serializer new type - Location (longitude, latitude)
     *
     * @return mixed
     */
    public function getLocation()
    {
        return null;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function isPrivate(): bool
    {
        return $this->private;
    }

    public function setPrivate(bool $private): self
    {
        $this->private = $private;

        return $this;
    }
}