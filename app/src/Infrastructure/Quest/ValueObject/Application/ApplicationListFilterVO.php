<?php

namespace App\Infrastructure\Quest\ValueObject\Application;

use App\Domain\Quest\ValueObject\QuestCommit\ApplicationListFilterVOInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class ApplicationListFilterVO
 */
class ApplicationListFilterVO implements ApplicationListFilterVOInterface
{
    /**
     * @Serializer\Type("int")
     *
     * @var int
     */
    private $take = 10;

    /**
     * @Serializer\Type("int")
     *
     * @var int
     */
    private $page = 1;

    /**
     * @var string
     */
    private $userId;

    /**
     * @Serializer\Type("string")
     *
     * @var string|null
     */
    private $status;

    /**
     * @return int
     */
    public function getTake(): int
    {
        return $this->take;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        $page = ($this->page > 1) ? $this->page : 1;

        return ($page - 1) * $this->take;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     *
     * @return ApplicationListFilterVOInterface
     */
    public function setUserId(string $userId): ApplicationListFilterVOInterface
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @param int $take
     *
     * @return ApplicationListFilterVOInterface
     */
    public function setTake(int $take): ApplicationListFilterVOInterface
    {
        $this->take = $take;

        return $this;
    }

    /**
     * @param int $page
     *
     * @return ApplicationListFilterVOInterface
     */
    public function setPage(int $page): ApplicationListFilterVOInterface
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     *
     * @return self
     */
    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }
}