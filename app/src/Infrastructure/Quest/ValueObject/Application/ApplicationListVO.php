<?php

namespace App\Infrastructure\Quest\ValueObject\Application;

use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Domain\Quest\ValueObject\QuestCommit\ApplicationListVOInterface;

/**
 * Class ApplicationListVO
 */
class ApplicationListVO implements ApplicationListVOInterface
{
    /**
     * @var int
     */
    private $total;

    /**
     * @var array
     */
    private $items;

    /**
     * @param int $total
     *
     * @return ApplicationListVOInterface
     */
    public function setTotal(int $total): ApplicationListVOInterface
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @param array $items
     *
     * @return ApplicationListVOInterface
     */
    public function setItems(array $items): ApplicationListVOInterface
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return GetApplicationInterface[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}