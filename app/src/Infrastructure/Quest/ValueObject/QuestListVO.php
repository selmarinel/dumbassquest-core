<?php

namespace App\Infrastructure\Quest\ValueObject;

use App\Domain\Quest\ValueObject\QuestListVOInterface;

/**
 * Class QuestListVO
 */
class QuestListVO implements QuestListVOInterface
{
    /**
     * @var int
     */
    private $total;

    /**
     * @var array
     */
    private $items;

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return self
     */
    public function setTotal(int $total): QuestListVOInterface
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     *
     * @return self
     */
    public function setItems(array $items): QuestListVOInterface
    {
        $this->items = $items;

        return $this;
    }
}