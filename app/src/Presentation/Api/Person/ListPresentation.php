<?php

declare(strict_types=1);

namespace App\Presentation\Api\Person;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class ListPresentation.
 */
class ListPresentation
{
    /**
     * @Serializer\Type("int")
     * @Serializer\Groups({"read"})
     *
     * @var int
     */
    private $total;

    /**
     *
     * @Serializer\Type("array")
     * @Serializer\Groups({"read"})
     *
     * @var ItemPresentation[]
     */
    private $items;

    public function __construct()
    {
        $this->total = 0;
        $this->items = [];
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return self
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return ItemPresentation[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param ItemPresentation $item
     *
     * @return self
     */
    public function addItem(ItemPresentation $item): self
    {
        $this->items[] = $item;

        return $this;
    }
}
