<?php

namespace App\Presentation\Api\Person;

use DateTime;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("NONE")
 *
 * @todo think about presentation and provide it to another services
 */
class ItemPresentation
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"full"})
     */
    private string $userId;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"full"})
     */
    private string $profileId;

    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"read", "full"})
     */
    private ?string $firstName;

    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"read", "full"})
     */
    private ?string $lastName;

    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"full"})
     */
    private ?string $email;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"read", "full"})
     */
    private string $username;

    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"full"})
     */
    private ?string $gender;

    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"full"})
     */
    private ?string $aboutMe;

    /**
     * @var DateTime|null
     *
     * @Serializer\Type("Datetime")
     * @Serializer\Groups({"full"})
     */
    private ?DateTime $birthDate;

    /**
     * @var bool
     *
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"read", "full"})
     */
    private bool $online;

    /**
     * @var array
     *
     * @Serializer\Type("array")
     * @Serializer\Groups({"read", "full"})
     */
    private array $features = [];

    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"read", "full"})
     */
    private string $avatar;

    /**
     * @Serializer\Type("integer")
     * @Serializer\Groups({"read", "full"})
     */
    private int $experience = 0;

    /**
     * @return bool
     */
    public function isOnline(): bool
    {
        return $this->online;
    }

    /**
     * @param bool $online
     *
     * @return self
     */
    public function setOnline(bool $online): self
    {
        $this->online = $online;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getBirthDate(): ?DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param DateTime|null $birthDate
     *
     * @return self
     */
    public function setBirthDate(?DateTime $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     *
     * @return self
     */
    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileId(): string
    {
        return $this->profileId;
    }

    /**
     * @param string $profileId
     *
     * @return self
     */
    public function setProfileId(string $profileId): self
    {
        $this->profileId = $profileId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     *
     * @return self
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     *
     * @return self
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return (string)$this->email;
    }

    /**
     * @param string|null $email
     *
     * @return self
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string|null $gender
     *
     * @return self
     */
    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAboutMe(): ?string
    {
        return $this->aboutMe;
    }

    /**
     * @param string|null $aboutMe
     *
     * @return self
     */
    public function setAboutMe(?string $aboutMe): self
    {
        $this->aboutMe = $aboutMe;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     *
     * @return self
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return array
     */
    public function getFeatures(): array
    {
        return $this->features;
    }

    /**
     * @param string $feature
     *
     * @return self
     */
    public function addFeature(string $feature): self
    {
        $this->features[] = $feature;

        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     *
     * @return self
     */
    public function setAvatar(string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return int
     */
    public function getExperience(): int
    {
        return $this->experience;
    }

    /**
     * @param int $experience
     *
     * @return self
     */
    public function setExperience(int $experience): self
    {
        $this->experience = $experience;

        return $this;
    }
}