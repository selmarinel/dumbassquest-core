<?php

namespace App\Presentation\Api\QuestCommit;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class ListPresentation
 */
class ListPresentation
{
    /**
     * @Serializer\Type("int")
     * @Serializer\Groups({"read"})
     *
     * @var int
     */
    private $total;

    /**
     *
     * @Serializer\Type("array")
     * @Serializer\Groups({"read"})
     *
     * @var ItemPresentation[]
     */
    private $items;

    /**
     * ListPresentation constructor.
     */
    public function __construct()
    {
        $this->total = 10;
        $this->items = [];
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return self
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return ItemPresentation[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param ItemPresentation $presentation
     *
     * @return ListPresentation
     */
    public function addItem(ItemPresentation $presentation): ListPresentation
    {
        $this->items[] = $presentation;

        return $this;
    }
}