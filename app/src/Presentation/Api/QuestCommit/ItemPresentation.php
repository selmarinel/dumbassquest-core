<?php

namespace App\Presentation\Api\QuestCommit;

use App\Presentation\Api\Quest\ItemPresentation as QuestItemPresentation;
use DateTime;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class QuestCommitPresentation
 */
class ItemPresentation
{
    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"read"})
     *
     * @var string
     */
    private $id;

    /**
     * @Serializer\Groups({"read"})
     *
     * @var QuestItemPresentation
     */
    private $quest;

    /**
     * @Serializer\Groups({"read"})
     *
     * @var DateTime
     */
    private $acceptedAt;

    /**
     * @Serializer\Groups({"read"})
     *
     * @var string
     */
    private $status;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return QuestItemPresentation
     */
    public function getQuest(): QuestItemPresentation
    {
        return $this->quest;
    }

    /**
     * @param QuestItemPresentation $quest
     *
     * @return self
     */
    public function setQuest(QuestItemPresentation $quest): self
    {
        $this->quest = $quest;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getAcceptedAt(): DateTime
    {
        return $this->acceptedAt;
    }

    /**
     * @param DateTime $acceptedAt
     *
     * @return self
     */
    public function setAcceptedAt(DateTime $acceptedAt): self
    {
        $this->acceptedAt = $acceptedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}