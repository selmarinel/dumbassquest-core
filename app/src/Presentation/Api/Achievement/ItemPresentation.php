<?php

declare(strict_types=1);

namespace App\Presentation\Api\Achievement;

use DateTimeInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class ItemPresentation.
 */
class ItemPresentation
{
    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"read"})
     *
     * @var string
     */
    private $id;

    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"read"})
     *
     * @var string
     */
    private $name;

    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"read"})
     *
     * @var string
     */
    private $description;

    /**
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"read"})
     *
     * @var boolean
     */
    private $isReceived;

    /**
     * @Serializer\Type("float")
     * @Serializer\Groups({"read"})
     * @Serializer\SkipWhenEmpty()
     *
     * @var float
     */
    private $progress;

    /**
     * @Serializer\Type("DateTime")
     * @Serializer\Groups({"read"})
     * @Serializer\SkipWhenEmpty();
     *
     * @var DateTimeInterface;
     */
    private $createdAt;

    /**
     * @Serializer\Type("DateTime")
     * @Serializer\Groups({"read"})
     * @Serializer\SkipWhenEmpty();
     *
     * @var DateTimeInterface;
     */
    private $receivedAt;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool
     */
    public function isReceived(): bool
    {
        return $this->isReceived;
    }

    /**
     * @param bool $isReceived
     *
     * @return self
     */
    public function setIsReceived(bool $isReceived): self
    {
        $this->isReceived = $isReceived;

        return $this;
    }

    /**
     * @return float
     */
    public function getProgress(): float
    {
        return $this->progress;
    }

    /**
     * @param float $progress
     *
     * @return self
     */
    public function setProgress(float $progress): self
    {
        $this->progress = $progress;

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeInterface $createdAt
     *
     * @return self
     */
    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getReceivedAt(): ?DateTimeInterface
    {
        return $this->receivedAt;
    }

    /**
     * @param DateTimeInterface|null $receivedAt
     *
     * @return self
     */
    public function setReceivedAt(?DateTimeInterface $receivedAt): self
    {
        $this->receivedAt = $receivedAt;

        return $this;
    }
}
