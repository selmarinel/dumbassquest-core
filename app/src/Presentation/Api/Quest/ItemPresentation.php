<?php

namespace App\Presentation\Api\Quest;

use DateTime;
use JMS\Serializer\Annotation as Serializer;
use App\Presentation\Api\Person\ItemPresentation as PersonItemPresentation;

/**
 * Class ItemPresentation
 */
class ItemPresentation
{
    /**
     * @Serializer\Groups({"read"})
     *
     * @var string
     */
    private string $id;

    /**
     * @Serializer\Groups({"read"})
     *
     * @var string
     */
    private string $title;

    /**
     * @Serializer\Groups({"read"})
     *
     * @var string
     */
    private string $description;

    /**
     * @Serializer\Groups({"read"})
     *
     * @var DateTime
     */
    private DateTime $created;

    /**
     * @Serializer\Groups({"read"})
     *
     * @var DateTime|null
     */
    private ?DateTime $expirationDate = null;

    /**
     * @Serializer\Groups({"read"})
     *
     * @var string
     */
    private string $type;

    /**
     * @Serializer\Groups({"read"})
     *
     * @var string
     */
    private string $status;

    /**
     * @Serializer\Groups({"read"})
     *
     * @var string
     */
    private string $icon;

    /**
     * @Serializer\Groups({"read"})
     *
     * @var bool
     */
    private bool $committed = false;

    /**
     * @Serializer\Groups({"read"})
     *
     * @var bool
     */
    private bool $completed = false;

    /**
     * @Serializer\Groups({"read"})
     */
    private PersonItemPresentation $person;

    /**
     * @Serializer\Groups({"read"})
     */
    private bool $private = false;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getExpirationDate(): ?DateTime
    {
        return $this->expirationDate;
    }

    /**
     * @param DateTime|null $expirationDate
     *
     * @return self
     */
    public function setExpirationDate(?DateTime $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return PersonItemPresentation
     */
    public function getPerson(): PersonItemPresentation
    {
        return $this->person;
    }

    /**
     * @param PersonItemPresentation $person
     *
     * @return self
     */
    public function setPerson(PersonItemPresentation $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;


        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;


        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     *
     * @return self
     */
    public function setCreated(DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     *
     * @return self
     */
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCommitted(): bool
    {
        return $this->committed;
    }

    /**
     * @param bool $committed
     *
     * @return self
     */
    public function setCommitted(bool $committed): self
    {
        $this->committed = $committed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->completed;
    }

    /**
     * @param bool $completed
     *
     * @return self
     */
    public function setCompleted(bool $completed): self
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPrivate(): bool
    {
        return $this->private;
    }

    /**
     * @param bool $private
     *
     * @return self
     */
    public function setPrivate(bool $private): self
    {
        $this->private = $private;

        return $this;
    }
}