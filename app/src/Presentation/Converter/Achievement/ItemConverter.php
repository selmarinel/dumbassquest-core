<?php

declare(strict_types=1);

namespace App\Presentation\Converter\Achievement;

use App\Domain\Achievement\DataTransferObject\AchievementDTOInterface;
use App\Domain\Achievement\DataTransferObject\ReceivedAchievementDTOInterface;
use App\Presentation\Api\Achievement\ItemPresentation;
use App\Presentation\Converter\PresentationConverterInterface;
use LogicException;

/**
 * Class ItemConverter.
 */
class ItemConverter implements PresentationConverterInterface
{
    /**
     * @return string
     */
    public function getPresentationClass(): string
    {
        return ItemPresentation::class;
    }

    /**
     * @param AchievementDTOInterface $achievement
     *
     * @return mixed
     */
    public function convert($achievement)
    {
        if (!$achievement instanceof AchievementDTOInterface) {
            throw new LogicException('invalid object in presentation');
        }

        $achievementPresentation = new ItemPresentation();

        $achievementPresentation
            ->setId($achievement->getId())
            ->setName($achievement->getName())
            ->setDescription($achievement->getDescription())
            ->setIsReceived(false);

        if ($achievement instanceof ReceivedAchievementDTOInterface) {
            $achievementPresentation
                ->setIsReceived(true)
                ->setProgress($achievement->getProgress())
                ->setCreatedAt($achievement->getCreatedAt())
                ->setReceivedAt($achievement->getReceivedAt());
        }

        return $achievementPresentation;
    }
}
