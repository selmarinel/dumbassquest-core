<?php

declare(strict_types=1);

namespace App\Presentation\Converter\Achievement;

use App\Domain\Achievement\DataTransferObject\AchievementDTOInterface;
use App\Presentation\Api\Achievement\ListPresentation;
use App\Presentation\Converter\PresentationConverterInterface;

/**
 * Class ListConverter.
 */
class ListConverter implements PresentationConverterInterface
{
    /**
     * @var ItemConverter
     */
    private $itemConverter;

    /**
     * ListConverter constructor.
     *
     * @param ItemConverter $itemConverter
     */
    public function __construct(ItemConverter $itemConverter)
    {
        $this->itemConverter = $itemConverter;
    }

    /**
     * @return string
     */
    public function getPresentationClass(): string
    {
        return ListPresentation::class;
    }

    /**
     * @param AchievementDTOInterface[] $list
     *
     * @return mixed
     */
    public function convert($list)
    {
        $result = [];

        /** @var AchievementDTOInterface $item */
        foreach ($list as $item) {
            $result[] = $this->itemConverter->convert($item);
        }

        return $result;
    }
}
