<?php

namespace App\Presentation\Converter\Person;

use App\Application\Service\RedisClient\CacheClientInterface;
use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Infrastructure\Person\Model\Profile;
use App\Presentation\Api\Person\ItemPresentation;
use App\Presentation\Converter\PresentationConverterInterface;
use LogicException;

/**
 * Class PersonPresentationConverter
 */
class ItemConverter implements PresentationConverterInterface
{
    private CacheClientInterface $cache;

    public function __construct(CacheClientInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return string
     */
    public function getPresentationClass(): string
    {
        return ItemPresentation::class;
    }

    /**
     * @param GetProfileInterface $person
     *
     * @return ItemPresentation
     */
    public function convert($person): ItemPresentation
    {
        /** @var Profile $person */
        if (!$person instanceof GetProfileInterface) {
            throw new LogicException('invalid object in presentation');
        }
        $presentation = new ItemPresentation();

        foreach ($person->getUser()->getFeatures() as $feature) {
            if ($feature->isAvailable()) {
                $presentation->addFeature($feature->getName());
            }
        }

        return $presentation
            ->setOnline($this->isOnline($person))
            ->setUserId($person->getUserId())
            ->setProfileId($person->getId())
            ->setFirstName($person->getFirstName())
            ->setLastName($person->getLastName())
            ->setBirthDate($person->getBirthDate())
            ->setGender($person->getGender())
            ->setAboutMe($person->getAboutMe())
            ->setUsername($person->getUser()->getUsername())
            ->setEmail($person->getUser()->getEmail())
            ->setExperience($person->getUser()->getExperience())
            ->setAvatar($person->getAvatar() ? : 'https://loremflickr.com/150/150');
    }

    /**
     * @param GetProfileInterface $person
     *
     * @return bool
     */
    private function isOnline(GetProfileInterface $person): bool
    {
        $isOnline = $this->cache->get("user:{$person->getUserId()}");

        if (!$isOnline) {
            return false;
        }

        return $isOnline === 'online';
    }
}