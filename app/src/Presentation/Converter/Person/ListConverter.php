<?php

declare(strict_types=1);

namespace App\Presentation\Converter\Person;

use App\Domain\Person\ValueObject\PersonListVOInterface;
use App\Presentation\Api\Person\ListPresentation;
use App\Presentation\Converter\PresentationConverterInterface;
use LogicException;

/**
 * Class ListConverter.
 */
class ListConverter implements PresentationConverterInterface
{
    /**
     * @var ItemConverter
     */
    private $converter;

    /**
     * ListConverter constructor.
     *
     * @param ItemConverter $converter
     */
    public function __construct(ItemConverter $converter)
    {
        $this->converter = $converter;
    }

    /**
     * @return string
     */
    public function getPresentationClass(): string
    {
        return ListPresentation::class;
    }

    /**
     * @param PersonListVOInterface $object
     *
     * @return ListPresentation
     */
    public function convert($object): ListPresentation
    {
        if (!$object instanceof PersonListVOInterface) {
            throw new LogicException('Unsupported class in converter');
        }

        $listPresentation = new ListPresentation();
        $listPresentation->setTotal($object->getTotal());
        foreach ($object->getItems() as $profile) {
            $listPresentation->addItem($this->converter->convert($profile));
        }

        return $listPresentation;
    }
}
