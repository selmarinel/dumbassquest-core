<?php

namespace App\Presentation\Converter;

/**
 * Interface PresentationConverterInterface
 */
interface PresentationConverterInterface
{
    /**
     * @return string
     */
    public function getPresentationClass(): string;

    /**
     * @param mixed $object
     *
     * @return mixed
     */
    public function convert($object);
}