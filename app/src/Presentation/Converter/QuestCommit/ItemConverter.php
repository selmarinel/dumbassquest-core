<?php

namespace App\Presentation\Converter\QuestCommit;

use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Presentation\Api\QuestCommit\ItemPresentation;
use App\Presentation\Converter\PresentationConverterInterface;
use App\Presentation\Converter\Quest\ItemConverter as QuestItemConverter;
use LogicException;

/**
 * Class ItemConverter
 */
class ItemConverter implements PresentationConverterInterface
{
    /**
     * @var QuestItemConverter
     */
    private $questItemConverter;

    /**
     * @param QuestItemConverter $questItemConverter
     */
    public function __construct(QuestItemConverter $questItemConverter)
    {
        $this->questItemConverter = $questItemConverter;
    }

    /**
     * @return string
     */
    public function getPresentationClass(): string
    {
        return ItemPresentation::class;
    }

    /**
     * @param GetApplicationInterface $application
     *
     * @return mixed
     */
    public function convert($application)
    {
        if (!$application instanceof GetApplicationInterface) {
            throw new LogicException('invalid object in presentation');
        }

        $presentation = new ItemPresentation();

        return $presentation
            ->setId($application->getId())
            ->setAcceptedAt($application->getAcceptedAt())
            ->setStatus($application->getStatus())
            ->setQuest($this->questItemConverter->convert($application->getQuest()));
    }
}