<?php

namespace App\Presentation\Converter\QuestCommit;

use App\Domain\Quest\ValueObject\QuestCommit\ApplicationListVOInterface;
use App\Infrastructure\Quest\Model\Application;
use App\Presentation\Api\QuestCommit\ListPresentation;
use App\Presentation\Converter\PresentationConverterInterface;
use LogicException;

/**
 * Class ListConverter
 */
class ListConverter implements PresentationConverterInterface
{
    /**
     * @var ItemConverter
     */
    private $itemConverter;

    /**
     * ListConverter constructor.
     * @param ItemConverter $itemConverter
     */
    public function __construct(ItemConverter $itemConverter)
    {
        $this->itemConverter = $itemConverter;
    }

    /**
     * @return string
     */
    public function getPresentationClass(): string
    {
        return ListPresentation::class;
    }

    /**
     * @param ApplicationListVOInterface $listVO
     *
     * @return mixed
     */
    public function convert($listVO): ListPresentation
    {
        if (!$listVO instanceof ApplicationListVOInterface) {
            throw new LogicException('Invalid object conversion');
        }

        $questCommitListPresentation = new ListPresentation();
        $questCommitListPresentation->setTotal($listVO->getTotal());

        /** @var Application $questCommit */
        foreach ($listVO->getItems() as $questCommit) {
            $questCommitListPresentation->addItem($this->itemConverter->convert($questCommit));
        }

        return $questCommitListPresentation;
    }
}