<?php

namespace App\Presentation\Converter\Quest;

use App\Domain\Quest\Dictionary\ApplicationStatus;
use App\Domain\Quest\Misc\UserStorageInterface;
use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Repository\ApplicationRepositoryInterface;
use App\Domain\Quest\ValueObject\QuestListVOInterface;
use App\Infrastructure\Quest\Model\Quest;
use App\Presentation\Api\Quest\ListPresentation;
use App\Presentation\Converter\PresentationConverterInterface;
use LogicException;

class ListConverter implements PresentationConverterInterface
{
    private ItemConverter $itemConverter;

    private ApplicationRepositoryInterface $applicationRepository;

    private UserStorageInterface $userStorage;

    public function __construct(
        ItemConverter $itemConverter,
        ApplicationRepositoryInterface $applicationRepository,
        UserStorageInterface $userStorage
    )
    {
        $this->itemConverter = $itemConverter;
        $this->applicationRepository = $applicationRepository;
        $this->userStorage = $userStorage;

    }

    public function getPresentationClass(): string
    {
        return ListPresentation::class;
    }

    /**
     * @param QuestListVOInterface $list
     *
     * @return ListPresentation
     */
    public function convert($list): ListPresentation
    {
        if (!$list instanceof QuestListVOInterface) {
            throw new LogicException('Invalid object conversion');
        }

        $questListPresentation = new ListPresentation();
        $questListPresentation->setTotal($list->getTotal());

        /** @var Quest $quest */
        foreach ($list->getItems() as $quest) {
            $questListPresentation->addItem($this->itemConverter->convert($quest));
        }

        return $this->prepareResult($list, $questListPresentation);
    }

    /**
     * @param QuestListVOInterface $listVO
     * @param ListPresentation $listPresentation
     *
     * @return ListPresentation
     */
    private function prepareResult(QuestListVOInterface $listVO, ListPresentation $listPresentation): ListPresentation
    {
        try {
            $userId = $this->userStorage->getUserId();

            $applications = $this->applicationRepository->findByUserIdAndQuests(
                $userId,
                array_map(static function (GetQuestInterface $quest) {
                    return $quest->getId();
                }, $listVO->getItems())
            );

            $completedQuests = [];
            $acceptedQuests = [];
            foreach ($applications as $application) {
                $quest = $application->getQuest();

                if ($application->getStatus() === ApplicationStatus::COMPLETED) {
                    $completedQuests[$quest->getId()] = $quest;
                }

                if ($application->getStatus() === ApplicationStatus::ACCEPTED) {
                    $acceptedQuests[$quest->getId()] = $quest;
                }
            }

            foreach ($listPresentation->getItems() as $itemPresentation) {
                $itemPresentation->setCommitted((bool)($acceptedQuests[$itemPresentation->getId()] ?? false));
                $itemPresentation->setCompleted((bool)($completedQuests[$itemPresentation->getId()] ?? false));
            }
        } catch (\Throwable $exception) {
            //todo log exception
        }

        return $listPresentation;
    }
}