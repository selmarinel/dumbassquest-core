<?php

namespace App\Presentation\Converter\Quest;

use App\Domain\Person\Service\PersonFetcherInterface;
use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Presentation\Api\Quest\ItemPresentation;
use App\Presentation\Converter\PresentationConverterInterface;
use App\Presentation\Converter\Person\ItemConverter as PersonItemConverter;
use LogicException;

/**
 * Class QuestPresentationConverter
 */
class ItemConverter implements PresentationConverterInterface
{
    private const DEFAULT_ICON = '/img/default.jpg';

    /**
     * @var PersonFetcherInterface
     */
    private PersonFetcherInterface $personFetcher;

    /**
     * @var PersonItemConverter
     */
    private PersonItemConverter $personConverter;

    /**
     * QuestPresentation constructor.
     * @param PersonFetcherInterface $personFetcher
     * @param PersonItemConverter $personConverter
     */
    public function __construct(PersonFetcherInterface $personFetcher, PersonItemConverter $personConverter)
    {
        $this->personFetcher = $personFetcher;
        $this->personConverter = $personConverter;
    }


    /**
     * @return string
     */
    public function getPresentationClass(): string
    {
        return ItemPresentation::class;
    }

    /**
     * @param GetQuestInterface $quest
     *
     * @return ItemPresentation
     */
    public function convert($quest): ItemPresentation
    {
        if (!$quest instanceof GetQuestInterface) {
            throw new LogicException('invalid object in presentation');
        }

        $questPresentation = new ItemPresentation();

        return $questPresentation
            ->setId($quest->getId())
            ->setStatus($quest->getStatus())
            ->setTitle($quest->getTitle())
            ->setDescription(nl2br($quest->getDescription()))
            ->setType($quest->getType())
            ->setCreated($quest->getCreated())
            ->setIcon($quest->getIcon() ?: self::DEFAULT_ICON)
            ->setPrivate($quest->isPrivate())
            ->setPerson(
                $this->personConverter->convert(
                    $this->personFetcher->fetchProfileByUserId($quest->getUserId())
                )
            );
    }
}