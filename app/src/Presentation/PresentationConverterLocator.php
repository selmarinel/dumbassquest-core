<?php

namespace App\Presentation;

use App\Application\Service\JMSMapper\JMSMapperConverter;
use App\Presentation\Converter\PresentationConverterInterface;

/**
 * Class PresentationLocator
 */
class PresentationConverterLocator
{
    /**
     * @var PresentationConverterInterface[]
     */
    private array $converters = [];

    /**
     * @var JMSMapperConverter
     */
    private JMSMapperConverter $mapperConverter;

    /**
     * PresentationFactory constructor.
     * @param JMSMapperConverter $mapperConverter
     * @param PresentationConverterInterface[] $converters
     */
    public function __construct(JMSMapperConverter $mapperConverter, iterable $converters)
    {
        $this->mapperConverter = $mapperConverter;
        foreach ($converters as $converter) {
            $this->addConverter($converter);
        }
    }

    /**
     * @param PresentationConverterInterface $presentationConverter
     */
    public function addConverter(PresentationConverterInterface $presentationConverter): void
    {
        $this->converters[$presentationConverter->getPresentationClass()] = $presentationConverter;
    }

    /**
     * @param $object
     * @param string $presentationClassname
     * @param array $groups
     *
     * @return array
     */
    public function convert($object, string $presentationClassname, array $groups = [JMSMapperConverter::READ_GROUP]): array
    {
        $converter = $this->converters[$presentationClassname];

        return $this->mapperConverter->mapToArray($converter->convert($object), $groups);
    }
}