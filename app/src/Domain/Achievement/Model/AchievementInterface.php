<?php

declare(strict_types=1);

namespace App\Domain\Achievement\Model;

/**
 * Interface AchievementInterface.
 */
interface AchievementInterface
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return string|null
     */
    public function getGrandType(): ?string;

    /**
     * @return string|null
     */
    public function getIcon(): ?string;

    /**
     * @param string $name
     *
     * @return AchievementInterface
     */
    public function setName(string $name): AchievementInterface;

    /**
     * @param string $description
     *
     * @return AchievementInterface
     */
    public function setDescription(string $description): AchievementInterface;
}
