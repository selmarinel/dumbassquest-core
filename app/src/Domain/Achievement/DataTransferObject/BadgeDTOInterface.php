<?php

declare(strict_types=1);

namespace App\Domain\Achievement\DataTransferObject;

/**
 * Interface BadgeDTO.
 */
interface BadgeDTOInterface
{
    public function getName(): string;

    public function getDescription(): string;
}
