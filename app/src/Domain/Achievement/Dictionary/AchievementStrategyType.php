<?php

declare(strict_types=1);

namespace App\Domain\Achievement\Dictionary;

/**
 * Class AchievementType.
 */
final class AchievementStrategyType
{
    public const ACTION = 'action';
    public const STATE = 'state';
    public const PROGRESS = 'progress';
}
