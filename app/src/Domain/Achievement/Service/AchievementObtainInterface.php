<?php

declare(strict_types=1);

namespace App\Domain\Achievement\Service;

/**
 * Interface AchievementGrandHandleInterface.
 */
interface AchievementObtainInterface
{
    /**
     * @param string $achievementName
     * @param string $userId
     */
    public function obtain(string $achievementName, string $userId): void;
}
