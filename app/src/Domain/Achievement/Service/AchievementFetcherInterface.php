<?php

declare(strict_types=1);

namespace App\Domain\Achievement\Service;

use App\Domain\Achievement\DataTransferObject\AchievementDTOInterface;
use App\Domain\Achievement\DataTransferObject\BadgeDTOInterface;

/**
 * Interface AchievementFetcherInterface.
 */
interface AchievementFetcherInterface
{
    /**
     * @param string $name
     *
     * @return AchievementDTOInterface
     */
    public function fetchAchievement(string $name): AchievementDTOInterface;

    /**
     * @param string $name
     *
     * @return BadgeDTOInterface
     */
    public function fetchBadge(string $name): BadgeDTOInterface;

    /**
     * @return AchievementDTOInterface[]
     */
    public function fetchAchievements(): array;
}
