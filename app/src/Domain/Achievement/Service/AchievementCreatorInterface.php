<?php

declare(strict_types=1);

namespace App\Domain\Achievement\Service;

use App\Domain\Achievement\DataTransferObject\AchievementDTOInterface;
use App\Domain\Achievement\DataTransferObject\BadgeDTOInterface;
use App\Domain\Achievement\Model\BadgeInterface;

/**
 * Interface AchievementSaver.
 */
interface AchievementCreatorInterface
{
    /**
     * @param AchievementDTOInterface $achievementDTO
     *
     * @return AchievementDTOInterface
     */
    public function createAchievement(AchievementDTOInterface $achievementDTO): AchievementDTOInterface;

    /**
     * @param BadgeDTOInterface $badgeDTO
     *
     * @return BadgeInterface
     */
    public function createBadge(BadgeDTOInterface $badgeDTO): BadgeInterface;

    /**
     * @param AchievementDTOInterface $achievement
     * @param BadgeDTOInterface $badge
     *
     * @return BadgeInterface
     */
    public function attachAchievementToBadge(
        AchievementDTOInterface $achievement,
        BadgeDTOInterface $badge
    ): BadgeInterface;
}
