<?php

declare(strict_types=1);

namespace App\Domain\Achievement\Service;

use App\Domain\Achievement\Model\AchievementReceivedInterface;

/**
 * Interface AchievementProcessorInterface.
 */
interface AchievementProcessorInterface
{
    /**
     * @return string
     */
    public function getProcessorType(): string;

    /**
     * @param string $achievementName
     * @param string $userId
     *
     * @return AchievementReceivedInterface
     */
    public function process(string $achievementName, string $userId): AchievementReceivedInterface;
}
