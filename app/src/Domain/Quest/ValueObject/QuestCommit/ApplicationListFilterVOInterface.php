<?php

namespace App\Domain\Quest\ValueObject\QuestCommit;

/**
 * Interface ApplicationListFilterVOInterface
 */
interface ApplicationListFilterVOInterface
{
    /**
     * @return int
     */
    public function getTake(): int;

    /**
     * @return int
     */
    public function getOffset(): int;

    /**
     * @return string
     */
    public function getUserId(): string;

    /**
     * @return string|null
     */
    public function getStatus(): ?string;

}