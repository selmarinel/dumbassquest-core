<?php

namespace App\Domain\Quest\ValueObject\QuestCommit;

use App\Domain\Quest\Model\Application\GetApplicationInterface;

/**
 * Interface QuestCommitListVOInterface
 */
interface ApplicationListVOInterface
{
    /**
     * @param int $total
     *
     * @return ApplicationListVOInterface
     */
    public function setTotal(int $total): ApplicationListVOInterface;

    /**
     * @param array $items
     *
     * @return ApplicationListVOInterface
     */
    public function setItems(array $items): ApplicationListVOInterface;

    /**
     * @return int
     */
    public function getTotal(): int;

    /**
     * @return GetApplicationInterface[]
     */
    public function getItems(): array;
}