<?php

namespace App\Domain\Quest\ValueObject;

use DateTime;

/**
 * Interface QuestVOInterface
 */
interface QuestVOInterface
{
    public function getId(): ?string;

    public function getTitle(): string;

    public function getDescription(): string;

    public function getExpirationDate(): ?DateTime;

    /**
     * todo return Location type
     * todo add to doctrine location type
     * add to serializer new type - Location (longitude, latitude)
     *
     * @return mixed
     */
    public function getLocation();

    public function getIcon(): ?string;

    public function isPrivate(): bool;
}