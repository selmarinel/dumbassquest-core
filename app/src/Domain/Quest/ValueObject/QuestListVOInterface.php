<?php

namespace App\Domain\Quest\ValueObject;

use App\Domain\Quest\Model\Quest\GetQuestInterface;

/**
 * Interface QuestListVOInterface
 */
interface QuestListVOInterface
{
    /**
     * @param int $total
     *
     * @return QuestListVOInterface
     */
    public function setTotal(int $total): QuestListVOInterface;

    /**
     * @param array $items
     *
     * @return QuestListVOInterface
     */
    public function setItems(array $items): QuestListVOInterface;

    /**
     * @return int
     */
    public function getTotal(): int;

    /**
     * @return GetQuestInterface[]
     */
    public function getItems(): array;
}