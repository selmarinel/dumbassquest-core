<?php

namespace App\Domain\Quest\ValueObject;

/**
 * Interface QuestListFilterVO
 */
interface QuestListFilterVOInterface
{
    /**
     * @return string|null
     */
    public function getQuery(): ?string;

    /**
     * @return int
     */
    public function getTake(): int;

    /**
     * @return int
     */
    public function getOffset(): int;

    /**
     * @return bool
     */
    public function isDeleted(): bool;

    /**
     * @return string[]
     */
    public function getType(): array;

    /**
     * @return string[]
     */
    public function getUsers(): array;
}