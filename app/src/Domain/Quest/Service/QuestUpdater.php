<?php

namespace App\Domain\Quest\Service;

use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Model\Quest\SetQuestInterface;
use App\Domain\Quest\Repository\QuestRepositoryInterface;
use App\Domain\Quest\ValueObject\QuestVOInterface;
use RuntimeException;

/**
 * Class QuestUpdater
 */
class QuestUpdater
{
    /**
     * @var QuestRepositoryInterface
     */
    private $questRepository;

    /**
     * QuestUpdater constructor.
     *
     * @param QuestRepositoryInterface $questRepository
     */
    public function __construct(QuestRepositoryInterface $questRepository)
    {
        $this->questRepository = $questRepository;
    }

    /**
     * @param QuestVOInterface $questVO
     *
     * @return GetQuestInterface
     */
    public function update(QuestVOInterface $questVO): GetQuestInterface
    {
        /**
         * @var SetQuestInterface $quest
         */
        if (!$quest = $this->questRepository->findById($questVO->getId())) {
            throw new RuntimeException('quest.not_found');
        }

        $quest
            ->setTitle($questVO->getTitle())
            ->setDescription($questVO->getDescription())
            ->setIcon($questVO->getIcon())
            ->setPrivate($questVO->isPrivate())
            ->setExpirationDate($questVO->getExpirationDate());

        return $this->questRepository->save($quest);
    }
}