<?php

namespace App\Domain\Quest\Service;

use App\Domain\Quest\ValueObject\QuestListFilterVOInterface;
use App\Domain\Quest\ValueObject\QuestListVOInterface;

/**
 * Interface QuestListInterface
 */
interface QuestListInterface
{
    /**
     * @param QuestListFilterVOInterface $questListFilterVO
     *
     * @return QuestListVOInterface
     */
    public function getList(QuestListFilterVOInterface $questListFilterVO): QuestListVOInterface;
}