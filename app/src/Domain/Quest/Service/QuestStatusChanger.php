<?php

namespace App\Domain\Quest\Service;

use App\Domain\Quest\Dictionary\QuestStatus;
use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Model\Quest\SetQuestInterface;
use App\Domain\Quest\Repository\QuestRepositoryInterface;
use Exception;

/**
 * Class QuestStatusChanger
 */
class QuestStatusChanger
{
    /**
     * @var QuestRepositoryInterface
     */
    private $questRepository;

    /**
     * QuestStatusChanger constructor.
     * @param QuestRepositoryInterface $questRepository
     */
    public function __construct(QuestRepositoryInterface $questRepository)
    {
        $this->questRepository = $questRepository;
    }

    /**
     * @param SetQuestInterface $quest
     *
     * @return GetQuestInterface
     *
     * @throws Exception
     */
    public function delete(SetQuestInterface $quest): GetQuestInterface
    {
        return $this->questRepository->save($quest->setStatus(QuestStatus::STATUS_DELETED));
    }

    /**
     * @param SetQuestInterface $quest
     *
     * @return GetQuestInterface
     */
    public function ban(SetQuestInterface $quest): GetQuestInterface
    {
        return $this->questRepository->save($quest->setStatus(QuestStatus::STATUS_BANNED));
    }

    /**
     * @param SetQuestInterface $quest
     * @param bool|null $active
     *
     * @return GetQuestInterface
     */
    public function switch(SetQuestInterface $quest, ?bool $active = true): GetQuestInterface
    {
        return $this->questRepository->save(
            $quest->setStatus($active ? QuestStatus::STATUS_ACTIVE : QuestStatus::STATUS_INACTIVE)
        );
    }
}