<?php

namespace App\Domain\Quest\Service;

use App\Domain\Quest\Factory\QuestFactoryInterface;
use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Repository\QuestRepositoryInterface;
use App\Domain\Quest\ValueObject\QuestVOInterface;

/**
 * Class QuestCreator
 */
class QuestCreator
{
    /**
     * @var QuestRepositoryInterface
     */
    private $questRepository;

    /**
     * @var QuestFactoryInterface
     */
    private $questFactory;

    /**
     * QuestCreator constructor.
     * @param QuestFactoryInterface $questFactory
     * @param QuestRepositoryInterface $questRepository
     */
    public function __construct(
        QuestFactoryInterface $questFactory,
        QuestRepositoryInterface $questRepository
    ) {
        $this->questFactory = $questFactory;
        $this->questRepository = $questRepository;
    }

    /**
     * @param QuestVOInterface $questVO
     *
     * @return GetQuestInterface
     */
    public function createQuest(QuestVOInterface $questVO): GetQuestInterface
    {
        return $this->questRepository->save($this->questFactory->createQuest($questVO));
    }
}