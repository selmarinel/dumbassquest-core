<?php

namespace App\Domain\Quest\Service\Checker;
use App\Domain\Quest\Model\Quest\GetQuestInterface;

/**
 * Interface CheckInterface
 */
interface CheckInterface
{
    /**
     * @param GetQuestInterface $quest
     *
     * @return bool
     */
    public function check(GetQuestInterface $quest): bool;
}