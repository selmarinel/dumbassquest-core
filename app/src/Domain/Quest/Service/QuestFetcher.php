<?php

namespace App\Domain\Quest\Service;

use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Repository\QuestRepositoryInterface;

/**
 * Class QuestFetcher
 */
class QuestFetcher
{
    /**
     * @var QuestRepositoryInterface
     */
    private $questRepository;

    /**
     * QuestFetcher constructor.
     * @param QuestRepositoryInterface $questRepository
     */
    public function __construct(QuestRepositoryInterface $questRepository)
    {
        $this->questRepository = $questRepository;
    }

    /**
     * @param string $id
     *
     * @return GetQuestInterface|null
     */
    public function fetchQuest(string $id): ?GetQuestInterface
    {
        return $this->questRepository->findById($id);
    }
}