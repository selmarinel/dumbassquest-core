<?php

namespace App\Domain\Quest\Service\Application;

use App\Domain\Quest\ValueObject\QuestCommit\ApplicationListFilterVOInterface;
use App\Domain\Quest\ValueObject\QuestCommit\ApplicationListVOInterface;

/**
 * Interface QuestCommitListInterface
 */
interface ApplicationListInterface
{
    /**
     * @param ApplicationListFilterVOInterface $filterVO
     *
     * @return ApplicationListVOInterface
     */
    public function getList(ApplicationListFilterVOInterface $filterVO): ApplicationListVOInterface;
}