<?php

namespace App\Domain\Quest\Service\Application\Workflow;

use App\Domain\Quest\Exception\CanNotCommitOnQuestException;
use App\Domain\Quest\Factory\ApplicationFactoryInterface;
use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Repository\ApplicationRepositoryInterface;
use App\Domain\Quest\Service\Checker\CheckInterface;

/**
 * Class QuestApplier
 */
class QuestApplier
{
    /**
     * @var ApplicationRepositoryInterface
     */
    private $commitRepository;

    /**
     * @var ApplicationFactoryInterface
     */
    private $questCommitFactory;

    /**
     * @var CheckInterface
     */
    private $check;

    /**
     * QuestConfirm constructor.
     *
     * @param ApplicationFactoryInterface $questCommitFactory
     * @param ApplicationRepositoryInterface $commitRepository
     * @param CheckInterface $check
     */
    public function __construct(
        ApplicationFactoryInterface $questCommitFactory,
        ApplicationRepositoryInterface $commitRepository,
        CheckInterface $check
    )
    {
        $this->questCommitFactory = $questCommitFactory;
        $this->commitRepository = $commitRepository;
        $this->check = $check;
    }

    /**
     * @param GetQuestInterface $quest
     *
     * @return GetApplicationInterface
     *
     * @throws CanNotCommitOnQuestException
     */
    public function apply(GetQuestInterface $quest): GetApplicationInterface
    {
        if (!$this->check->check($quest)) {
            throw new CanNotCommitOnQuestException($quest);
        }

        $questCommit = $this->questCommitFactory->createNewQuestApplication($quest);

        return $this->commitRepository->save($questCommit);
    }
}