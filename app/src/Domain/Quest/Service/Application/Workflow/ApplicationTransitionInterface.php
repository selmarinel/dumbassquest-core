<?php

namespace App\Domain\Quest\Service\Application\Workflow;

use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Domain\Quest\Model\Application\SetApplicationInterface;

/**
 * Interface ApplicationTransitionInterface
 */
interface ApplicationTransitionInterface
{
    /**
     * @param SetApplicationInterface $application
     *
     * @return GetApplicationInterface
     */
    public function transit(SetApplicationInterface $application): GetApplicationInterface;

    /**
     * @return string
     */
    public function getTransitionName(): string;
}