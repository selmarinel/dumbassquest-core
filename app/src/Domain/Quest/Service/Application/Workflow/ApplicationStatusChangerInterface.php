<?php

namespace App\Domain\Quest\Service\Application\Workflow;

use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Domain\Quest\Model\Application\SetApplicationInterface;

/**
 * Interface ApplicationStatusChangerInterface
 */
interface ApplicationStatusChangerInterface
{
    /**
     * @param string $action
     * @param SetApplicationInterface $application
     *
     * @return GetApplicationInterface
     */
    public function transit(string $action, SetApplicationInterface $application): GetApplicationInterface;
}