<?php

declare(strict_types=1);

namespace App\Domain\Quest\Dictionary;

/**
 * Class QuestCommitStatus.
 */
final class ApplicationStatus
{
    public const ACCEPTED = 'accepted';
    public const IN_PROGRESS = 'in_progress';
    public const FROZEN = 'frozen';
    public const COMPLETED = 'completed';
    public const DECLINED = 'declined';
    public const FAILED = 'failed';
}
