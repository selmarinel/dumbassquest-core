<?php

namespace App\Domain\Quest\Dictionary;

/**
 * Class QuestType
 */
final class QuestType
{
    public const SYSTEM_TYPE_NEW = 'new';

    public const SYSTEM_TYPE_HOT = 'hot';

    public const PERSON_TYPE_GROUP = 'group';

    public const PERSON_TYPE_SINGLE = 'single';

    public const OFTEN_TYPE_ON_TIME = 'one_time';

    public const OFTEN_TYPE_REGULAR = 'regular';
}