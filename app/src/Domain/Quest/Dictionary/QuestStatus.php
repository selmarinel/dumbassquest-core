<?php

namespace App\Domain\Quest\Dictionary;

/**
 * Class QuestStatus
 */
final class QuestStatus
{
    public const STATUS_ACTIVE = 'active';

    public const STATUS_INACTIVE = 'inactive';

    public const STATUS_BANNED = 'banned';

    public const STATUS_DELETED = 'deleted';
}