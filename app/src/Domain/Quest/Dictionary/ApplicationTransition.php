<?php

declare(strict_types=1);

namespace App\Domain\Quest\Dictionary;

/**
 * Class ApplicationTransition.
 */
final class ApplicationTransition
{
    public const ACCEPT = 'accept';
    public const START = 'start';
    public const COMPLETE = 'complete';
    public const DECLINE = 'decline';

    /** @deprecated */
    public const FREEZE = 'freeze';
    /** @deprecated */
    public const UN_FREEZE = 'freeze';
    /** @deprecated */
    public const FAIL = 'fail';
}
