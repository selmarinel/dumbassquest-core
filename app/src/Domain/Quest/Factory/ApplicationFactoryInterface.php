<?php

declare(strict_types=1);

namespace App\Domain\Quest\Factory;

use App\Domain\Quest\Model\Application\SetApplicationInterface;
use App\Domain\Quest\Model\Quest\GetQuestInterface;

/**
 * interface QuestCommitFactoryInterface.
 */
interface ApplicationFactoryInterface
{
    /**
     * @param GetQuestInterface $quest
     *
     * @return SetApplicationInterface
     */
    public function createNewQuestApplication(GetQuestInterface $quest): SetApplicationInterface;
}
