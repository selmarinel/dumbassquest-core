<?php

declare(strict_types=1);

namespace App\Domain\Quest\Factory;

use App\Domain\Quest\Model\Quest\SetQuestInterface;
use App\Domain\Quest\ValueObject\QuestVOInterface;

/**
 * Interface QuestFactoryInterface.
 */
interface QuestFactoryInterface
{
    /**
     * @param QuestVOInterface $questVO
     *
     * @return SetQuestInterface
     */
    public function createQuest(QuestVOInterface $questVO): SetQuestInterface;
}
