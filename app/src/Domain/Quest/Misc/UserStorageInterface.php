<?php

declare(strict_types=1);

namespace App\Domain\Quest\Misc;

use App\Domain\Person\Model\User\GetUserInterface;

interface UserStorageInterface
{
    public function getUserId(): string;

    public function getUser(): GetUserInterface;
}