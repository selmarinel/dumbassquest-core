<?php

declare(strict_types=1);

namespace App\Domain\Quest\Exception;

/**
 * Class CanNotCommitOnQuestException.
 */
class CanNotCommitOnQuestException extends QuestException
{
}
