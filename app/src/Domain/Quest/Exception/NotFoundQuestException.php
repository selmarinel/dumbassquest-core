<?php

declare(strict_types=1);

namespace App\Domain\Quest\Exception;

/**
 * Class NotFoundQuestException.
 */
class NotFoundQuestException extends QuestException
{
    protected $message = 'quest.not_found';
}
