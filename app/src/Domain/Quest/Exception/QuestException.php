<?php

declare(strict_types=1);

namespace App\Domain\Quest\Exception;

use App\Domain\Quest\Model\Quest\GetQuestInterface;
use Exception;
use Throwable;

/**
 * Class QuestException.
 */
class QuestException extends Exception
{
    /**
     * @var GetQuestInterface
     */
    private $quest;

    public function __construct(GetQuestInterface $quest, $message = '', $code = 0, Throwable $previous = null)
    {
        $this->quest = $quest;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return GetQuestInterface
     */
    public function getQuest(): GetQuestInterface
    {
        return $this->quest;
    }
}
