<?php

namespace App\Domain\Quest\Repository;

use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Model\Quest\SetQuestInterface;

/**
 * Interface QuestRepositoryInterface
 */
interface QuestRepositoryInterface
{
    /**
     * @param SetQuestInterface $quest
     *
     * @return GetQuestInterface
     */
    public function save(SetQuestInterface $quest): GetQuestInterface;

    /**
     * @param string $id
     *
     * @return GetQuestInterface|null
     */
    public function findById(string $id): ?GetQuestInterface;
}