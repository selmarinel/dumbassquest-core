<?php

namespace App\Domain\Quest\Repository;

use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Domain\Quest\Model\Application\SetApplicationInterface;
use App\Domain\Quest\Model\Quest\GetQuestInterface;

/**
 * Interface QuestCommitRepositoryInterface
 */
interface ApplicationRepositoryInterface
{
    /**
     * @param SetApplicationInterface $questCommit
     *
     * @return GetApplicationInterface
     */
    public function save(SetApplicationInterface $questCommit): GetApplicationInterface;

    /**
     * @param string $questCommitId
     *
     * @return GetApplicationInterface|null
     */
    public function findById(string $questCommitId): ?GetApplicationInterface;

    /**
     * @param string $userId
     *
     * @return GetApplicationInterface[]
     */
    public function findByUserId(string $userId): array;

    /**
     * @param GetQuestInterface $quest
     * @param string $userId
     *
     * @return GetApplicationInterface|null
     */
    public function findByQuestIdAndUserId(GetQuestInterface $quest, string $userId): ?GetApplicationInterface;

    /**
     * @param string $userId
     * @param array $questIds
     *
     * @return GetApplicationInterface[]
     */
    public function findByUserIdAndQuests(string $userId, array $questIds): array;
}