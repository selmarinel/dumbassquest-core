<?php

namespace App\Domain\Quest\Model\Quest;

use DateTime;

interface SetQuestInterface
{
    public function setType(string $type): SetQuestInterface;

    public function setUserId(string $userId): SetQuestInterface;

    public function setStatus(string $status): SetQuestInterface;

    public function setTitle(string $title): SetQuestInterface;

    public function setDescription(string $description): SetQuestInterface;

    public function setExpirationDate(?DateTime $expirationDate): SetQuestInterface;

    public function setCreated(DateTime $created): SetQuestInterface;

    public function setIcon(string $icon): SetQuestInterface;

    public function setPrivate(bool $private): SetQuestInterface;
}