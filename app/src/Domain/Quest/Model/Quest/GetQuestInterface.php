<?php

namespace App\Domain\Quest\Model\Quest;

interface GetQuestInterface
{
    public function getId(): ?string;

    public function getUserId(): string;

    public function getStatus(): string;

    public function getIcon(): ?string;

    public function isPrivate(): bool;
}