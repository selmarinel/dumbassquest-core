<?php

namespace App\Domain\Quest\Model\Application;

use App\Domain\Quest\Model\Quest\GetQuestInterface;
use DateTime;

/**
 * Interface SetQuestCommitInterface
 */
interface SetApplicationInterface
{
    /**
     * @param string $userId
     *
     * @return SetApplicationInterface
     */
    public function setUserId(string $userId): SetApplicationInterface;

    /**
     * @param GetQuestInterface $quest
     *
     * @return SetApplicationInterface
     */
    public function setQuest(GetQuestInterface $quest): SetApplicationInterface;

    /**
     * @param DateTime $acceptedAt
     *
     * @return SetApplicationInterface
     */
    public function setAcceptedAt(DateTime $acceptedAt): SetApplicationInterface;
}