<?php

namespace App\Domain\Quest\Model\Application;

use App\Domain\Quest\Model\Quest\GetQuestInterface;
use DateTime;

/**
 * Interface GetApplicationInterface
 */
interface GetApplicationInterface
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return GetQuestInterface
     */
    public function getQuest(): GetQuestInterface;

    /**
     * @return DateTime
     */
    public function getAcceptedAt(): DateTime;

    /**
     * @return string
     */
    public function getStatus(): string;
}