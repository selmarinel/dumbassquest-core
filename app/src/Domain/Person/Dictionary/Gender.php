<?php

namespace App\Domain\Person\Dictionary;

/**
 * Class Gender
 */
final class Gender
{
    public const GENDER_UNDEFINED = 'alien';
    public const GENDER_MALE = 'male';
    public const GENDER_FEMALE = 'female';

    /**
     * @return array
     */
    public static function getGenders(): array
    {
        return [
            self::GENDER_UNDEFINED,
            self::GENDER_MALE,
            self::GENDER_FEMALE
        ];
    }
}