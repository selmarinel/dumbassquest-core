<?php

declare(strict_types=1);

namespace App\Domain\Person\Dictionary;

/**
 * Class Feature.
 */
final class Feature
{
    public const HIGHLIGHT_AVATAR  = 'highlight_avatar';
    public const HIGHLIGHT_NICKNAME = 'highlight_nickname';
}
