<?php

namespace App\Domain\Person\Service;

use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Domain\Person\ValueObject\UserVOInterface;

/**
 * Interface PersonSaverInterface
 */
interface PersonSaverInterface
{
    /**
     * @param UserVOInterface $userVO
     *
     * @return GetProfileInterface
     */
    public function savePerson(UserVOInterface $userVO): GetProfileInterface;
}