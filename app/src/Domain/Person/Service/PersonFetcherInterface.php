<?php

namespace App\Domain\Person\Service;

use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Domain\Person\Model\User\GetUserInterface;

/**
 * Class PersonFetcherInterface
 */
interface PersonFetcherInterface
{
    /**
     * @return GetUserInterface
     */
    public function fetchUser(): GetUserInterface;

    /**
     * @return GetProfileInterface
     */
    public function fetchProfile(): GetProfileInterface;

    /**
     * @param string $userId
     *
     * @return GetProfileInterface
     */
    public function fetchProfileByUserId(string $userId): GetProfileInterface;
}