<?php

namespace App\Domain\Person\Service;

use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Domain\Person\ValueObject\ProfileVOInterface;

/**
 * Interface ProfileUpdaterInterface
 */
interface ProfileUpdaterInterface
{
    /**
     * @param ProfileVOInterface $profileVO
     *
     * @return GetProfileInterface
     */
    public function updateProfile(ProfileVOInterface $profileVO): GetProfileInterface;
}