<?php

namespace App\Domain\Person\ValueObject;

/**
 * Interface GetUserVOInterface
 */
interface UserVOInterface
{
    /**
     * @return string
     */
    public function getUsername(): string;

    /**
     * @return string|null
     */
    public function getEmail(): ?string;

    /**
     * @return string|null
     */
    public function getPhone(): ?string;

    /**
     * @return string
     */
    public function getPassword(): string;
}