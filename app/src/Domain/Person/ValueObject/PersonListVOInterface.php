<?php

namespace App\Domain\Person\ValueObject;

/**
 * Interface PersonListVOInterface
 */
interface PersonListVOInterface
{
    /**
     * @return int
     */
    public function getTotal(): int;

    /**
     * @return ProfileVOInterface[]
     */
    public function getItems(): array;
}