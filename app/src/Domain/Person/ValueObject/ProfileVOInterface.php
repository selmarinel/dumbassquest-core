<?php

namespace App\Domain\Person\ValueObject;

use DateTime;

/**
 * Interface ProfileVOInterface
 */
interface ProfileVOInterface
{
    /**
     * @return string|null
     */
    public function getFirstName(): ?string;

    /**
     * @return string|null
     */
    public function getLastName(): ?string;

    /**
     * @return string|null
     */
    public function getGender(): ?string;

    /**
     * @return string|null
     */
    public function getAddress1(): ?string;

    /**
     * @return string|null
     */
    public function getCountry(): ?string;

    /**
     * @return string|null
     */
    public function getCity(): ?string;

    /**
     * @return string|null
     */
    public function getUserId(): ?string;

    /**
     * @return DateTime|null
     */
    public function getBirthDate(): ?DateTime;

    /**
     * @return string|null
     */
    public function getAboutMe(): ?string;

}