<?php

namespace App\Domain\Person\Repository;

use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Domain\Person\Model\Profile\SetProfileInterface;

/**
 * Interface ProfileRepositoryInterface
 */
interface ProfileRepositoryInterface
{
    /**
     * @param string $userId
     *
     * @return GetProfileInterface|null
     */
    public function findByUserId(string $userId): ?GetProfileInterface;

    /**
     * @param SetProfileInterface $user
     *
     * @return GetProfileInterface
     */
    public function save(SetProfileInterface $user): GetProfileInterface;
}