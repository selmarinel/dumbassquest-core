<?php

namespace App\Domain\Person\Repository;

use App\Domain\Person\Model\User\GetUserInterface;
use App\Domain\Person\Model\User\SetUserInterface;

/**
 * Interface UserRepositoryInterface
 */
interface UserRepositoryInterface
{
    /**
     * @param string $id
     *
     * @return GetUserInterface|null
     */
    public function findById(string $id): ?GetUserInterface;

    /**
     * @param string $username
     *
     * @return GetUserInterface|null
     */
    public function findByUsername(string $username): ?GetUserInterface;

    /**
     * @param SetUserInterface $user
     *
     * @return GetUserInterface
     */
    public function save(SetUserInterface $user): GetUserInterface;
}