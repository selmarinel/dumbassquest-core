<?php

namespace App\Domain\Person\Model\Profile;

use App\Domain\Person\Model\User\SetUserInterface;

/**
 * Interface SetProfileInterface
 */
interface SetProfileInterface
{
    /**
     * @param SetUserInterface $user
     *
     * @return SetProfileInterface
     */
    public function setUser(SetUserInterface $user): self;
}