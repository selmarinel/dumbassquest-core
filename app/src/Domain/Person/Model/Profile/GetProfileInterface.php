<?php

namespace App\Domain\Person\Model\Profile;

use DateTime;

/**
 * Interface GetProfileInterface
 */
interface GetProfileInterface
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getUserId(): string;

    /**
     * @return string|null
     */
    public function getFirstName(): ?string;

    /**
     * @return string|null
     */
    public function getLastName(): ?string;

    /**
     * @return string
     */
    public function getGender(): string;

    /**
     * @return DateTime|null
     */
    public function getBirthDate(): ?DateTime;

    /**
     * @return string|null
     */
    public function getAboutMe(): ?string;
}