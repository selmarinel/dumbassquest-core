<?php

namespace App\Domain\Person\Model\User;

/**
 * Interface SetUserInterface
 */
interface SetUserInterface
{
    /**
     * @param string $id
     * @return SetUserInterface
     */
    public function setId(string $id): self;

    /**
     * @param string $username
     * @return SetUserInterface
     */
    public function setUsername(string $username): self;
}