<?php

namespace App\Domain\Person\Model\User;

/**
 * Interface UserInterface
 */
interface GetUserInterface
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getUsername(): string;
}