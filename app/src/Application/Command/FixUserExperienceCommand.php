<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Infrastructure\Person\Model\User;
use App\Infrastructure\Person\Model\UserExperience;
use App\Infrastructure\Person\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixUserExperienceCommand extends Command
{
    private UserRepository $userRepository;

    protected static $defaultName = 'app:fix:user:experience';

    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct(null);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sql = <<<SQL
SELECT u.*
FROM users u
WHERE (SELECT ue.id FROM user_experience ue WHERE ue.user_id = u.id) is NULL
LIMIT :limit OFFSET :offset
SQL;

        $offset = 0;
        $limit = 10;
        gc_enable();
        do {
            $rsm = new ResultSetMapping();
            $rsm->addEntityResult(User::class, 'u');
            $rsm->addFieldResult('u', 'id', 'id');

            $users = $this->em->createNativeQuery($sql, $rsm)
                ->setParameter('limit', $limit)
                ->setParameter('offset', $offset)
                ->getResult();

            foreach ($users as $user) {
                $output->writeln(sprintf('<comment>Process Users %s</comment>', $user->getId()));

                if (null === $user->getUserExperience()) {
                    $userExperience = new UserExperience();
                    $userExperience
                        ->setUser($user)
                        ->setExperience(0);

                    $this->em->persist($userExperience);
                }
            }
            $offset += $limit;
            gc_collect_cycles();
        } while (!empty($users));

        $this->em->flush();
    }
}
