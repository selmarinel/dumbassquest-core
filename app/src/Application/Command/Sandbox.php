<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Domain\Quest\Repository\ApplicationRepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Sandbox.
 */
class Sandbox extends Command
{
    protected static $defaultName = 'sandbox';
    /**
     * @var ApplicationRepositoryInterface
     */
    private $repository;

    /**
     * Sandbox constructor.
     * @param ApplicationRepositoryInterface $repository
     */
    public function __construct(ApplicationRepositoryInterface $repository)
    {
        $this->repository = $repository;
        parent::__construct(null);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    }
}
