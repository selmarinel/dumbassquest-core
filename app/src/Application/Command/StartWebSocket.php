<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Application\Service\RedisClient\CacheClientInterface;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Swoole\Http\Request;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;
use swoole_websocket_server;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * Class StartWebSocket.
 */
class StartWebSocket extends Command
{
    /**
     * @var Server
     */
    private $webSocketServer;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * StartWebSocket constructor.
     * @param CacheClientInterface $cache
     */
    public function __construct(CacheClientInterface $cache)
    {
        $this->cache = $cache;

        $this->webSocketServer = new swoole_websocket_server('0.0.0.0', 9502, SWOOLE_SOCK_TCP);

        $this->webSocketServer->on('open', function ($ws, Request $request): void {
            $this->onConnection($request);
        });

        $this->webSocketServer->on('message', function ($ws, Frame $frame): void {
            $this->onMessage($frame);
        });

        $this->webSocketServer->on('close', function ($ws, $fd): void {
            $this->onClose($fd);
        });

        $this->webSocketServer->on('workerStart', function ($ws): void {
            $this->onWorkerStart($ws);
        });

        parent::__construct(null);
    }

    protected static $defaultName = 'web:socket:start';

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $output->writeln('Starting...');
        $this->webSocketServer->start();
    }

    /**
     * Client connected
     * @param Request $request
     */
    private function onConnection(Request $request): void
    {
        $this->output->writeln("Open connection {$request->fd}");
        $this->webSocketServer->push($request->fd, json_encode(['message' => 'Connection Init'], JSON_THROW_ON_ERROR, 512));
    }


    /**
     * @param Server $server
     */
    public function onWorkerStart(Server $server): void
    {
        $server->tick(25000, static function () use ($server) {
            foreach ($server->connections as $id) {
                $server->push($id, 'ping', WEBSOCKET_OPCODE_PING);
            }
        });
    }

    /**
     * @param Frame $frame
     *
     * @throws InvalidArgumentException
     */
    public function onMessage(Frame $frame): void
    {
        $data = json_decode($frame->data, true, 512, JSON_THROW_ON_ERROR);

        if (!isset($data['type'])) {
            $this->webSocketServer->push(
                $frame->fd,
                json_encode(['message' => 'Invalid message data'], JSON_THROW_ON_ERROR, 512));

            return;
        }
        try {
            switch ($data['type']) {
                case 'ONLINE':
                    $userId = $data['data']['user_id'];

                    $this->cache->set("connection:user:map:{$frame->fd}", $userId);
                    $this->cache->set("user:{$userId}", 'online');

                    $this->webSocketServer->push(
                        $frame->fd,
                        json_encode(['message' => 'User Online'], JSON_THROW_ON_ERROR, 512)
                    );

                    break;
                case 'OFFLINE':
                    $this->makeUserOfflineFromFrame($frame->fd);

                    break;
            }
        } catch (Exception $exception) {
            $this->webSocketServer->push(
                $frame->fd,
                json_encode(
                    [
                        'message' => 'Error',
                        'data'    => $exception->getMessage(),
                    ],
                    JSON_THROW_ON_ERROR,
                    512
                )
            );
        }
    }

    /**
     * @param int $frameId
     *
     * @throws InvalidArgumentException
     */
    public function onClose(int $frameId): void
    {
        $this->output->writeln("Closed connection {$frameId}");
        $this->makeUserOfflineFromFrame($frameId);
    }

    /**
     * @param int $frameId
     *
     * @throws InvalidArgumentException
     */
    private function makeUserOfflineFromFrame(int $frameId): void
    {
        if ($userId = $this->cache->get("connection:user:map:{$frameId}")) {
            $this->cache->delete("user:{$userId}");
        }
        $this->cache->delete("connection:user:map:{$frameId}");
    }
}
