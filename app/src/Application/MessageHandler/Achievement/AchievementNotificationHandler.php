<?php

declare(strict_types=1);

namespace App\Application\MessageHandler\Achievement;

use App\Application\Message\Achievement\AchievementNotification;
use App\Infrastructure\Achievement\Service\AchievementObtainHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class AchievementNotificationHandler.
 */
class AchievementNotificationHandler implements MessageHandlerInterface
{
    /**
     * @var AchievementObtainHandler
     */
    private $achievementObtained;

    /**
     * AchievementNotificationHandler constructor.
     *
     * @param AchievementObtainHandler $achievementObtainHandler
     */
    public function __construct(AchievementObtainHandler $achievementObtainHandler)
    {
        $this->achievementObtained = $achievementObtainHandler;
    }

    public function __invoke(AchievementNotification $achievementNotification)
    {
        $this->achievementObtained->obtain($achievementNotification->getAchievementName(), $achievementNotification->getUserId());
    }
}
