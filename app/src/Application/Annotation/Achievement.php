<?php

declare(strict_types=1);

namespace App\Application\Annotation;

use Symfony\Component\HttpFoundation\Response;

/**
 * @Annotation
 */
class Achievement
{
    public string $achievementName;

    public int $statusCode = Response::HTTP_OK;
}
