<?php

declare(strict_types=1);

namespace App\Application\Annotation;

use App\Application\Service\JMSMapper\JMSMapperConverter;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Presentation.
 * @Annotation
 */
class Presentation
{
    /** @var string */
    public string $presentationClass;

    /** @var int */
    public int $statusCode = Response::HTTP_OK;

    /** @var array  */
    public array $groups = [JMSMapperConverter::READ_GROUP];
}
