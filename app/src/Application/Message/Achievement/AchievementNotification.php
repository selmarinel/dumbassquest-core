<?php

declare(strict_types=1);

namespace App\Application\Message\Achievement;

/**
 * Class AchievementNotification.
 */
class AchievementNotification
{
    /**
     * @var string
     */
    private $achievementName;

    /**
     * @var string|null
     */
    private $userId;

    /**
     * AchievementNotification constructor.
     *
     * @param string $achievementName
     * @param string|null $userId
     */
    public function __construct(string $achievementName, ?string $userId)
    {
        $this->achievementName = $achievementName;
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getAchievementName(): string
    {
        return $this->achievementName;
    }

    /**
     * @return string
     */
    public function getUserId(): ?string
    {
        return $this->userId;
    }
}
