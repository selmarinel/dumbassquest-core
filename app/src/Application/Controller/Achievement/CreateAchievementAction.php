<?php

declare(strict_types=1);

namespace App\Application\Controller\Achievement;

use App\Domain\Achievement\Service\AchievementCreatorInterface;
use App\Infrastructure\Achievement\DataTransferObject\AchievementDTO;
use App\Presentation\Api\Achievement\ItemPresentation;
use App\Presentation\PresentationConverterLocator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CreateAchievementAction.
 */
class CreateAchievementAction
{
    /**
     * @var AchievementCreatorInterface
     */
    private $creator;

    /**
     * @var PresentationConverterLocator
     */
    private $converterLocator;

    /**
     * CreateAchievementAction constructor.
     *
     * @param AchievementCreatorInterface $achievementCreator
     * @param PresentationConverterLocator $converterLocator
     */
    public function __construct(AchievementCreatorInterface $achievementCreator, PresentationConverterLocator $converterLocator)
    {
        $this->creator = $achievementCreator;
        $this->converterLocator = $converterLocator;
    }

    /**
     * @Route(path="/achievements", methods={"POST"}, name="create_achievement")
     *
     * @ParamConverter(name="achievementDTO", class=AchievementDTO::class, converter="mapper_converter", options={"groups":{"create"}})
     *
     * @param AchievementDTO $achievementDTO
     *
     * @return JsonResponse
     */
    public function __invoke(AchievementDTO $achievementDTO): JsonResponse
    {
        return new JsonResponse(
            $this->converterLocator->convert(
                $this->creator->createAchievement($achievementDTO),
                ItemPresentation::class
            )
        );
    }
}
