<?php

declare(strict_types=1);

namespace App\Application\Controller\Achievement;

use App\Infrastructure\Achievement\DataTransferObject\AchievementDTO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FetchAchievementByIdAction.
 */
class FetchAchievementByIdAction
{
    /**
     * @Route(path="/achievements/{id}", methods={"GET"}, name="fetch_achievement")
     *
     * @ParamConverter(name="achievementDTO", class=AchievementDTO::class, converter="mapper_converter")
     *
     * @param AchievementDTO $achievementDTO
     *
     * @return JsonResponse
     */
    public function __invoke(AchievementDTO $achievementDTO): JsonResponse
    {
        return new JsonResponse([
            "message" => 'not.supported',
            "data"    => [
                'id' => $achievementDTO->getId(),
            ]
        ], JsonResponse::HTTP_I_AM_A_TEAPOT);
    }
}
