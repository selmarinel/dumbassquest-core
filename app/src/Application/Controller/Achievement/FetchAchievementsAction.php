<?php

declare(strict_types=1);

namespace App\Application\Controller\Achievement;

use App\Infrastructure\Achievement\Service\AchievementFetcherFactory;
use App\Presentation\Api\Achievement\ListPresentation;
use App\Presentation\PresentationConverterLocator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class FetchAchievementsAction.
 */
class FetchAchievementsAction
{
    /**
     * @var PresentationConverterLocator
     */
    private $converterLocator;

    /**
     * @var Security
     */
    private $security;

    /**
     * FetchAchievementsAction constructor.
     *
     * @param PresentationConverterLocator $converterLocator
     * @param Security $security
     */
    public function __construct(PresentationConverterLocator $converterLocator, Security $security)
    {
        $this->security = $security;
        $this->converterLocator = $converterLocator;
    }

    /**
     * @Route(path="/achievements", methods={"GET"}, name="fetch_achievements")
     *
     * @param AchievementFetcherFactory $achievementFetcherFactory
     *
     * @return JsonResponse
     */
    public function __invoke(AchievementFetcherFactory $achievementFetcherFactory): JsonResponse
    {
        $fetchType = $this->security->isGranted('IS_AUTHENTICATED_FULLY') ?
            AchievementFetcherFactory::ACHIEVEMENTS_FOR_USER :
            AchievementFetcherFactory::ACHIEVEMENTS;

        return new JsonResponse(
            $this->converterLocator->convert(
                $achievementFetcherFactory
                    ->getFetcher($fetchType)
                    ->fetchAchievements(),
                ListPresentation::class
            )
        );
    }
}
