<?php

namespace App\Application\Controller;

use App\Infrastructure\Person\Event\GrandExperienceEvent;
use App\Infrastructure\Person\Model\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class IndexController
 *
 */
class IndexController extends AbstractController
{
    /**
     * @IsGranted("IS_AUTHENTICATED_ANONYMOUSLY")
     *
     * @Route(path="/", name="core_index", methods={"GET"})
     * @return Response
     */
    public function getIndexAction(): Response
    {
        return $this->json([
            'message' => 'CORE API v1.0.0'
        ]);
    }
}