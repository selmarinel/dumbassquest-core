<?php

namespace App\Application\Controller\Application\Workflow;

use App\Application\Annotation\Presentation;
use App\Domain\Quest\Exception\CanNotCommitOnQuestException;
use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Domain\Quest\Service\Application\Workflow\QuestApplier;
use App\Infrastructure\Quest\Model\Quest;
use App\Presentation\Api\QuestCommit\ItemPresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class QuestApplyAction
 */
final class QuestApplyAction
{
    /**
     * @var QuestApplier
     */
    private $applier;

    /**
     * QuestApplyAction constructor.
     *
     * @param QuestApplier $questApplier
     */
    public function __construct(QuestApplier $questApplier)
    {
        $this->applier = $questApplier;
    }

    /**
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     *
     * @Route(path="/quests/{id}/apply", methods={"POST"}, name="apply to quest")
     *
     * @param Quest $quest
     *
     * @return GetApplicationInterface
     *
     * @throws CanNotCommitOnQuestException
     *
     * @Presentation(ItemPresentation::class)
     */
    public function __invoke(Quest $quest): GetApplicationInterface
    {
        return $this->applier->apply($quest);
    }
}