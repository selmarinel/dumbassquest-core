<?php

declare(strict_types=1);

namespace App\Application\Controller\Application\Workflow;

use App\Infrastructure\Quest\Voter\ApplicationVoter;
use App\Application\Annotation\Presentation;
use App\Domain\Quest\Model\Application\GetApplicationInterface;
use App\Infrastructure\Quest\Model\Application;
use App\Infrastructure\Quest\Service\Application\ApplicationTransitionFactory;
use App\Presentation\Api\QuestCommit\ItemPresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;

final class TransitQuestApplicationAction
{
    private ApplicationTransitionFactory $factory;

    public function __construct(ApplicationTransitionFactory $applicationTransitionFactory)
    {
        $this->factory = $applicationTransitionFactory;
    }

    /**
     * @IsGranted(ApplicationVoter::CAN_TRANSIT_APPLICATION, subject="application")
     *
     * @Route(
     *     path="/quests/applications/{id}/{transition}",
     *     methods={"GET"},
     *     name="transit_quest_application",
     *     requirements={"transition" = "start|complete|decline"}
     * )
     *
     * @param Application $application
     * @param string $transition
     *
     * @return GetApplicationInterface
     *
     * @Presentation(ItemPresentation::class)
     */
    public function __invoke(Application $application, string $transition): GetApplicationInterface
    {
        return $this->factory
            ->getTransition($transition)
            ->transit($application);
    }
}
