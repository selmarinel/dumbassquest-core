<?php

namespace App\Application\Controller\Application;

use App\Application\Annotation\Presentation;
use App\Domain\Quest\Misc\UserStorageInterface;
use App\Domain\Quest\Service\Application\ApplicationListInterface;
use App\Domain\Quest\ValueObject\QuestCommit\ApplicationListVOInterface;
use App\Infrastructure\Quest\ValueObject\Application\ApplicationListFilterVO;
use App\Presentation\Api\QuestCommit\ListPresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MyApplicationsAction
 */
class MyApplicationsAction
{
    /**
     * @var ApplicationListInterface
     */
    private $list;
    /**
     * @var UserStorageInterface
     */
    private $userStorage;

    /**
     * MyApplicationsAction constructor.
     *
     * @param ApplicationListInterface $applicationList
     * @param UserStorageInterface $userStorage
     */
    public function __construct(ApplicationListInterface $applicationList, UserStorageInterface $userStorage)
    {
        $this->list = $applicationList;
        $this->userStorage = $userStorage;
    }

    /**
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     *
     * @Route(path="/quests/applications", name="my_applications", methods={"GET"})
     *
     * @ParamConverter(name="applicationListFilterVO", class=ApplicationListFilterVO::class, converter="mapper_converter")
     *
     * @param ApplicationListFilterVO $applicationListFilterVO
     *
     * @return ApplicationListVOInterface
     *
     * @Presentation(ListPresentation::class)
     */
    public function __invoke(ApplicationListFilterVO $applicationListFilterVO): ApplicationListVOInterface
    {
        $applicationListFilterVO->setUserId($this->userStorage->getUserId());

        return $this->list->getList($applicationListFilterVO);
    }
}
