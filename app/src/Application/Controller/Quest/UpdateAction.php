<?php

namespace App\Application\Controller\Quest;

use App\Application\Annotation\Presentation;
use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Service\QuestUpdater;
use App\Infrastructure\Quest\Security\QuestOwnerVoter;
use App\Infrastructure\Quest\ValueObject\QuestVO;
use App\Presentation\Api\Quest\ItemPresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class UpdateAction
 */
class UpdateAction
{
    /**
     * @var Security
     */
    private $security;
    /**
     * @var QuestUpdater
     */
    private $updater;

    /**
     * UpdateAction constructor.
     *
     * @param Security $security
     * @param QuestUpdater $questUpdater
     */
    public function __construct(Security $security, QuestUpdater $questUpdater)
    {
        $this->security = $security;
        $this->updater = $questUpdater;
    }

    /**
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     *
     * @Route(path="/quests/{id}", name="update_quest", methods={"PUT"})
     *
     * @ParamConverter(name="questVO", class=QuestVO::class, converter="mapper_converter", options={"groups":{"update"}})
     *
     * @param QuestVO $questVO
     *
     * @return GetQuestInterface
     *
     * @Presentation(ItemPresentation::class)
     */
    public function __invoke(QuestVO $questVO): GetQuestInterface
    {
        if (!$this->security->isGranted(QuestOwnerVoter::IS_QUEST_OWNER, $questVO->getId())) {
            throw new AccessDeniedHttpException();
        }

        return $this->updater->update($questVO);
    }
}