<?php

namespace App\Application\Controller\Quest;

use App\Application\Annotation\Presentation;
use App\Domain\Quest\Service\QuestListInterface;
use App\Domain\Quest\ValueObject\QuestListVOInterface;
use App\Infrastructure\Quest\ValueObject\QuestFilterVO;
use App\Presentation\Api\Quest\ListPresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ListAction
 */
class ListAction
{
    private QuestListInterface $list;

    /**
     * ListAction constructor.
     *
     * @param QuestListInterface $questList
     */
    public function __construct(QuestListInterface $questList)
    {
        $this->list = $questList;
    }

    /**
     * @Route(path="/quests", name="quests_list", methods={"GET"})
     *
     * @ParamConverter(name="questFilterVO", class=QuestFilterVO::class, converter="mapper_converter")
     *
     * @param QuestFilterVO $questFilterVO
     *
     * @return QuestListVOInterface
     *
     * @Presentation(ListPresentation::class)
     */
    public function __invoke(QuestFilterVO $questFilterVO): QuestListVOInterface
    {
        return $this->list->getList($questFilterVO);
    }
}