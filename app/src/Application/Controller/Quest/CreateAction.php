<?php

namespace App\Application\Controller\Quest;

use App\Application\Annotation\Presentation;
use App\Domain\Quest\Model\Quest\GetQuestInterface;
use App\Domain\Quest\Service\QuestCreator;
use App\Infrastructure\Quest\ValueObject\QuestVO;
use App\Presentation\Api\Quest\ItemPresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CreateAction
 */
class CreateAction
{
    /**
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     *
     * @Route(path="/quests", methods={"POST"}, name="create_quest")
     *
     * @ParamConverter(name="questVO", class=QuestVO::class, converter="mapper_converter", options={"groups":{"create"}})
     *
     * @param QuestVO $questVO
     * @param QuestCreator $questCreator
     *
     * @return GetQuestInterface
     *
     * @Presentation(ItemPresentation::class, statusCode=201)
     */
    public function __invoke(QuestVO $questVO, QuestCreator $questCreator): GetQuestInterface
    {
        return $questCreator->createQuest($questVO);
    }
}