<?php

namespace App\Application\Controller\Quest;

use App\Application\Annotation\Presentation;
use App\Infrastructure\Quest\Model\Quest;
use App\Infrastructure\Quest\Security\QuestVoter;
use App\Presentation\Api\Quest\ItemPresentation;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class GetQuestAction
 */
class GetAction
{
    /**
     * @Route(path="/quests/{id}", methods={"GET"}, name="get_quest")
     *
     * @param Quest $quest
     * @param Security $security
     *
     * @return Quest
     *
     * @Presentation(presentationClass=ItemPresentation::class)
     */
    public function __invoke(Quest $quest, Security $security): Quest
    {
        if (!$security->isGranted(QuestVoter::CAN_GET_QUEST, $quest)) {
            throw new AccessDeniedHttpException();
        }

        return $quest;
    }
}