<?php

namespace App\Application\Controller\Quest;

use App\Domain\Quest\Service\QuestStatusChanger;
use App\Infrastructure\Quest\Model\Quest;
use App\Infrastructure\Quest\Security\QuestOwnerVoter;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class DeleteAction
 */
class DeleteAction
{
    /**
     * @var QuestStatusChanger
     */
    private $statusChanger;
    /**
     * @var Security
     */
    private $security;

    /**
     * DeleteAction constructor.
     *
     * @param QuestStatusChanger $questStatusChanger
     * @param Security $security
     */
    public function __construct(QuestStatusChanger $questStatusChanger, Security $security)
    {
        $this->statusChanger = $questStatusChanger;
        $this->security = $security;
    }

    /**
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     *
     * @Route(path="/quests/{id}", methods={"DELETE"}, name="delete_quest")
     *
     * @param Quest $quest
     *
     * @return JsonResponse
     *
     * @throws Exception
     */
    public function __invoke(Quest $quest): JsonResponse
    {
        if (!$this->security->isGranted(QuestOwnerVoter::IS_QUEST_OWNER, $quest->getId())) {
            throw new AccessDeniedHttpException();
        }
        $this->statusChanger->delete($quest);

        return new JsonResponse([], JsonResponse::HTTP_NO_CONTENT);
    }
}