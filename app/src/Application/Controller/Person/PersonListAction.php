<?php

declare(strict_types=1);

namespace App\Application\Controller\Person;

use App\Application\Annotation\Presentation;
use App\Domain\Person\ValueObject\PersonListVOInterface;
use App\Infrastructure\Person\DataTransferObject\PersonFilterRequestDTO;
use App\Infrastructure\Person\Service\LazyLoadPerson;
use App\Presentation\Api\Person\ListPresentation;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PersonListAction.
 */
class PersonListAction
{
    /**
     * @var LazyLoadPerson
     */
    private LazyLoadPerson $personLoader;

    /**
     * PersonListAction constructor.
     *
     * @param LazyLoadPerson $lazyLoadPerson
     */
    public function __construct(LazyLoadPerson $lazyLoadPerson)
    {
        $this->personLoader = $lazyLoadPerson;
    }

    /**
     * @Route("/users", methods={"GET"})
     *
     * @param PersonFilterRequestDTO $personFilterRequestDTO
     *
     * @return PersonListVOInterface
     *
     * @Presentation(ListPresentation::class)
     */
    public function __invoke(PersonFilterRequestDTO $personFilterRequestDTO): PersonListVOInterface
    {
        return $this->personLoader->search($personFilterRequestDTO);
    }
}
