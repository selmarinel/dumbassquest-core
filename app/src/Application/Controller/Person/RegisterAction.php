<?php

namespace App\Application\Controller\Person;

use App\Application\Annotation\Presentation;
use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Domain\Person\Service\PersonSaverInterface;
use App\Infrastructure\Person\ValueObject\CreatePersonVO;
use App\Presentation\Api\Person\ItemPresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RegisterAction
 */
class RegisterAction
{
    /**
     * @var PersonSaverInterface
     */
    private PersonSaverInterface $saver;

    /**
     * RegisterAction constructor.
     *
     * @param PersonSaverInterface $saver
     */
    public function __construct(PersonSaverInterface $saver)
    {
        $this->saver = $saver;
    }

    /**
     * @Route(path="register", methods={"POST"}, name="register")
     *
     * @ParamConverter(name="createPersonVO", class=CreatePersonVO::class, converter="mapper_converter")
     *
     * @param CreatePersonVO $createPersonVO
     *
     * @return GetProfileInterface
     *
     * @Presentation(ItemPresentation::class)
     */
    public function __invoke(CreatePersonVO $createPersonVO): GetProfileInterface
    {
        return $this->saver->savePerson($createPersonVO);
    }
}