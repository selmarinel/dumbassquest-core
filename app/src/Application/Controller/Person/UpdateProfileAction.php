<?php

namespace App\Application\Controller\Person;

use App\Application\Annotation\Presentation;
use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Infrastructure\Person\Service\ProfileUpdater;
use App\Infrastructure\Person\ValueObject\UserProfileVO;
use App\Presentation\Api\Person\ItemPresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UpdateProfileAction
 */
class UpdateProfileAction
{
    /**
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     *
     * @Route(path="/me", name="update_profile", methods={"PUT"})
     *
     * @ParamConverter(name="userProfileVO", class=UserProfileVO::class, converter="mapper_converter")
     *
     * @param UserProfileVO $userProfileVO
     * @param ProfileUpdater $profileUpdater
     *
     * @return GetProfileInterface
     *
     * @Presentation(ItemPresentation::class)
     */
    public function __invoke(UserProfileVO $userProfileVO, ProfileUpdater $profileUpdater): GetProfileInterface
    {
        return $profileUpdater->updateProfile($userProfileVO);
    }
}