<?php

declare(strict_types=1);

namespace App\Application\Controller\Person;

use App\Infrastructure\Person\ValueObject\CheckUsernameVO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(path="/usernames/{username}", methods={"GET"}, name="check_username")
 */
class CheckUsernameAction
{
    /**
     * @ParamConverter(name="checkUsernameVO", class=CheckUsernameVO::class, converter="mapper_converter")
     *
     * @param CheckUsernameVO $checkUsernameVO
     *
     * @return JsonResponse
     */
    public function __invoke(CheckUsernameVO $checkUsernameVO): JsonResponse
    {
        return new JsonResponse([], JsonResponse::HTTP_NO_CONTENT);
    }
}
