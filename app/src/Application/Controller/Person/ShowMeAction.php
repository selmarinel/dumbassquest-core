<?php

namespace App\Application\Controller\Person;

use App\Application\Annotation\Achievement;
use App\Application\Annotation\Presentation;
use App\Domain\Person\Model\Profile\GetProfileInterface;
use App\Domain\Person\Service\PersonFetcherInterface;
use App\Presentation\Api\Person\ItemPresentation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ShowMeAction
 */
class ShowMeAction
{
    /**
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     *
     * @Route(path="/me", name="me", methods={"GET"})
     *
     * @param PersonFetcherInterface $personFetcher
     *
     * @Achievement("Register")
     *
     * @return GetProfileInterface
     *
     * @Presentation(ItemPresentation::class, groups={"full"})
     */
    public function __invoke(PersonFetcherInterface $personFetcher): GetProfileInterface
    {
        return $personFetcher->fetchProfile();
    }
}