<?php

declare(strict_types=1);

namespace App\Application\Misc;

use App\Domain\Person\Model\User\GetUserInterface;
use App\Domain\Quest\Misc\UserStorageInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class UserStorage.
 */
class UserStorage implements UserStorageInterface
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function getUserId(): string
    {
        return $this->getUser()->getId();
    }

    public function getUser(): GetUserInterface
    {
        /** @var GetUserInterface $user */
        $user = $this->security->getUser();
        if (!$user) {
            throw new \LogicException('user.not_auth');
        }

        return $user;
    }
}
