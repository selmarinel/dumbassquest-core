<?php

declare(strict_types=1);

namespace App\Application\EventListener;

use App\Application\Annotation\Achievement;
use App\Application\Message\Achievement\AchievementNotification;
use App\Domain\Quest\Misc\UserStorageInterface;
use Doctrine\Common\Annotations\Reader;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Messenger\MessageBusInterface;
use Throwable;

/**
 * Class AchievementSubscriber.
 */
class AchievementSubscriber implements EventSubscriberInterface
{
    private Reader $annotationReader;
    private MessageBusInterface $messageBus;
    private UserStorageInterface $userStorage;

    /**
     * AchievementSubscriber constructor.
     *
     * @param Reader $annotationReader
     * @param MessageBusInterface $messageBus
     * @param UserStorageInterface $userStorage
     */
    public function __construct(Reader $annotationReader, MessageBusInterface $messageBus, UserStorageInterface $userStorage)
    {
        $this->annotationReader = $annotationReader;
        $this->messageBus = $messageBus;
        $this->userStorage = $userStorage;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'obtainAchievement',
            KernelEvents::RESPONSE   => 'onKernelResponse',
        ];
    }

    /**
     * @param ResponseEvent $event
     */
    public function onKernelResponse(ResponseEvent $event): void
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        /** @var Achievement $achievement */
        $achievement = $event->getRequest()->attributes->get('__achievement');

        if (!$achievement instanceof Achievement){
            return;
        }

        if ($event->getResponse()->getStatusCode() !== $achievement->statusCode) {
            return;
        }

        try {
            $userId = $this->userStorage->getUserId();

            if ($achievement->achievementName && $event->isMasterRequest()) {
                $this->messageBus->dispatch(new AchievementNotification($achievement->achievementName, $userId));
            }
        } catch (Throwable $exception) {
            return;
        }
    }

    /**
     * @param ControllerEvent $controllerEvent
     *
     * @throws ReflectionException
     */
    public function obtainAchievement(ControllerEvent $controllerEvent)
    {
        try {
            $action = new ReflectionMethod($controllerEvent->getController(), '__invoke');

            $achievement = $this
                ->annotationReader
                ->getMethodAnnotation(
                    $action,
                    Achievement::class
                );


            if ($achievement instanceof Achievement) {
                $controllerEvent->getRequest()->attributes->set('__achievement', $achievement);
            }
        } catch (Throwable $exception) {
            return;
        }
    }
}
