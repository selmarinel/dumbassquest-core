<?php

namespace App\Application\EventListener;

use App\Domain\Quest\Exception\CanNotCommitOnQuestException;
use App\Domain\Quest\Exception\NotFoundQuestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ExceptionListener
 */
class ExceptionListener
{
    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        if (getenv('APP_ENV') === 'dev') {
            return;
        }

        // You get the exception object from the received event
        $exception = $event->getException();

        $response = new JsonResponse();
        $response->setData([
            'code'    => $exception->getCode(),
            'message' => $exception->getMessage(),
            'trace'   => $exception->getTrace()
        ]);


        switch (true) {
            case ($exception instanceof CanNotCommitOnQuestException) :
                $response->setStatusCode(JsonResponse::HTTP_FORBIDDEN);
                $response->setData([
                    'message' => 'quest.commit.forbidden'
                ]);
                break;
            case ($exception instanceof NotFoundQuestException):
            case  ($exception instanceof NotFoundHttpException):
                $response->setStatusCode(JsonResponse::HTTP_NOT_FOUND);
                $response->setData([
                    'message' => 'not_found'
                ]);
                break;
            default:
                $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);
        }

        // sends the modified response object to the event
        $event->setResponse($response);
    }
}