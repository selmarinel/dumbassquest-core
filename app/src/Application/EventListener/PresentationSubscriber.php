<?php

declare(strict_types=1);

namespace App\Application\EventListener;

use App\Application\Annotation\Presentation;
use App\Presentation\PresentationConverterLocator;
use Doctrine\Common\Annotations\Reader;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class PresentationSubscriber.
 */
class PresentationSubscriber implements EventSubscriberInterface
{
    /**
     * @var PresentationConverterLocator
     */
    private PresentationConverterLocator $converterLocator;

    /**
     * @var Reader
     */
    private Reader $annotationReader;

    /**
     * PresentationSubscriber constructor.
     *
     * @param Reader $annotationReader
     * @param PresentationConverterLocator $converterLocator
     */
    public function __construct(Reader $annotationReader, PresentationConverterLocator $converterLocator)
    {
        $this->annotationReader = $annotationReader;
        $this->converterLocator = $converterLocator;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
            KernelEvents::VIEW       => 'onKernelView',
        ];
    }

    /**
     * @param ControllerEvent $event
     */
    public function onKernelController(ControllerEvent $event): void
    {
        try {
            $action = new ReflectionMethod($event->getController(), '__invoke');
            $presentation = $this
                ->annotationReader
                ->getMethodAnnotation(
                    $action,
                    Presentation::class
                );

            $event->getRequest()->attributes->set('__presentation', $presentation);
        } catch (ReflectionException $e) {
            //todo
        }
    }

    /**
     * @param ViewEvent $event
     */
    public function onKernelView(ViewEvent $event): void
    {
        $request = $event->getRequest();
        if ($request->attributes->has('__presentation')) {
            /** @var Presentation $presentation */
            $presentation = $request->attributes->get('__presentation');

            $event->setResponse(
                new JsonResponse(
                    $this->converterLocator->convert(
                        $event->getControllerResult(),
                        $presentation->presentationClass,
                        $presentation->groups
                    ),
                    $presentation->statusCode
                )
            );
        }
    }
}
