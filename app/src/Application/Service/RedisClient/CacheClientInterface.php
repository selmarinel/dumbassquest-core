<?php

declare(strict_types=1);

namespace App\Application\Service\RedisClient;

/**
 * Interface CacheClientInterface.
 */
interface CacheClientInterface
{
    /**
     * @param string $key
     *
     * @return string|null
     */
    public function get(string $key): ?string;

    /**
     * @param string $key
     * @param string $data
     * @param int $expire
     */
    public function set(string $key, string $data, int $expire = 3000): void;

    /**
     * @param string $key
     */
    public function delete(string $key): void;
}
