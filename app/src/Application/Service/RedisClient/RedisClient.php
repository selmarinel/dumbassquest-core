<?php

declare(strict_types=1);

namespace App\Application\Service\RedisClient;

use Predis\Client;

/**
 * Class RedisClient.
 */
class RedisClient implements CacheClientInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * RedisClient constructor.
     *
     * @param string $redisUrl
     */
    public function __construct(string $redisUrl)
    {
        $this->client = new Client($redisUrl);
    }

    /**
     * @param string $key
     *
     * @return string|null
     */
    public function get(string $key): ?string
    {
        return $this->client->get($key);
    }

    /**
     * @param string $key
     * @param string $data
     * @param int $expire
     */
    public function set(string $key, string $data, int $expire = 3000): void
    {
        $this->client->set($key, $data);
        $this->client->expire($key, $expire);
    }

    /**
     * @param string $key
     */
    public function delete(string $key): void
    {
        $this->client->del([$key]);
    }
}
