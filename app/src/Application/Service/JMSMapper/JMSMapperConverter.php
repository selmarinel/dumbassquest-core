<?php

namespace App\Application\Service\JMSMapper;

use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;

/**
 * todo send to bundle
 *
 * Class JMSMapperConverter
 */
class JMSMapperConverter
{
    public const READ_GROUP = 'read';

    /** @var SubscribingHandlerInterface[] */
    private $handlers = [];

    /**
     * //todo add destination class for process handlers by tags
     * @param SubscribingHandlerInterface $handler
     */
    public function addHandler(SubscribingHandlerInterface $handler)
    {
        $this->handlers[] = $handler;
    }

    /**
     * @return SubscribingHandlerInterface[]
     */
    public function getHandlers(): array
    {
        return $this->handlers;
    }

    /**
     * @param $object
     * @param array $groups
     * @return mixed
     */
    public function mapToArray($object, array $groups = [])
    {
        $context = SerializationContext::create();

        if ($groups) {
            $context->setGroups($groups);
        }

        $builder = SerializerBuilder::create()->configureHandlers(function (HandlerRegistry $registry) {
            foreach ($this->getHandlers() as $handler) {
                $registry->registerSubscribingHandler($handler);
            }
        });

        return $builder->build()->toArray($object, $context);
    }
}