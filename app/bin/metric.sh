#!/usr/bin/env bash

rm -rf ./var/cache/report

php ./vendor/bin/phpmetrics $1 --report-html=./var/cache/report