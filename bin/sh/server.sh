#!/usr/bin/env bash

COMPOSE_FILES="-f bin/compose/docker-compose.yaml -f bin/compose/docker-compose.prod.yaml"

NAME_PREFIX="duq"

echo 'Copy env file'
cp ./bin/env/default.env ./app/.env
echo 'Success'

docker-compose -p $NAME_PREFIX $COMPOSE_FILES up -d --build

docker exec duq_core php bin/console swoole:server:stop

docker exec duq_core php bin/console c:c --no-debug

docker exec duq_core php bin/console swoole:server:start --port=88 --host=0.0.0.0 --no-interaction --no-ansi --env=dev --no-debug