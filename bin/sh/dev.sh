#!/usr/bin/env bash

COMPOSE_FILES="-f bin/compose/docker-compose.yaml -f bin/compose/docker-compose.dev.yaml"

NAME_PREFIX="duq"

echo 'Copy env file'
cp ./bin/env/default.env ./app/.env
echo 'Success'

docker-compose -p $NAME_PREFIX $COMPOSE_FILES stop
docker-compose -p $NAME_PREFIX $COMPOSE_FILES build
docker-compose -p $NAME_PREFIX $COMPOSE_FILES up -d

docker exec duq_api composer install
docker exec duq_api /usr/bin/supervisord

echo 'DEV Containers successfuly runned'
sleep 5