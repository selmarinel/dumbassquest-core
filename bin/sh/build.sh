#!/usr/bin/env bash

COMPOSE_FILES="-f bin/compose/docker-compose.yaml"

NAME_PREFIX="duq"

echo 'Copy env file'
cp ./bin/env/default.env ./app/.env
echo 'Success'

docker-compose -p $NAME_PREFIX $COMPOSE_FILES rm -f
docker-compose -p $NAME_PREFIX $COMPOSE_FILES build --pull
docker-compose -p $NAME_PREFIX $COMPOSE_FILES up -d --force-recreate

docker exec -it duq_core composer install