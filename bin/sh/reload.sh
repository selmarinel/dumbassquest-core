#!/usr/bin/env bash

COMPOSE_FILES="-f bin/compose/docker-compose.yaml -f bin/compose/docker-compose.dev.yaml"

NAME_PREFIX="duq"

docker-compose -p $NAME_PREFIX $COMPOSE_FILES stop

docker-compose -p $NAME_PREFIX $COMPOSE_FILES up -d --build